const webpack = require('webpack');
const withCSS = require('@zeit/next-css');

require('dotenv').config();

module.exports = withCSS({
  cssModules: true,
  cssLoaderOptions: {
    importLoaders: 1,
    localIdentName: "[local]___[hash:base64:5]"
  },
  webpack: (config) => {
    // Fixes npm packages that depend on `fs` module
    config.node = {
      fs: 'empty'
    }

    // configures dotenv
    config.plugins.push(
      new webpack.EnvironmentPlugin({
        'API_URL': 'http://api.radio.etl.yt',
        'I18N_URL': null,
        'PLAYER_STORAGE_KEY': 'RadiostanicaPlayerPrefs',
        'FAVORITES_STORAGE_KEY': 'RadiostanicaFavorites',
        'HISTORY_STORAGE_KEY': 'RadiostanicaHistory',
        'STORAGE_STATIONS_LIMIT': '10',
        'PLAYER_QUERY_VAR': 'play'
      })
    );

    return config;
  }
});
