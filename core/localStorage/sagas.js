import { eventChannel } from 'redux-saga';
import { put, takeLatest, take, select, fork, call } from 'redux-saga/effects';
import { localStorageActionTypes as actionTypes, failGet, failSet, successGet, successSet, storageInitialized } from './actions';

import storageAvailable from '../../util/storage-available';
import { sourceId, storageKey } from '../sync-middleware';

function* setLocalDataSaga({ payload }) {
  try {
    if (!storageAvailable('localStorage'))
      throw new Error('localStorage not available');
    if (!payload || !payload.value)
      throw new Error('Value not specified');

    const prop = payload.key || 'untitled';
    window.localStorage.setItem(prop, JSON.stringify(payload.value));
    yield put(successSet({ [prop]: payload.value }));
  } catch (e) {
    yield put(failSet(e.name + ': ' + e.message));
  }
}

function* getLocalDataSaga({ payload }) {
  try {
    if (!storageAvailable('localStorage'))
      throw new Error('localStorage not available');

    if (payload && payload.prop) {
      const data = window.localStorage.getItem(payload.prop);
      yield put(successGet({ [payload.prop]: JSON.parse(data) }));
    } else {
      // initial storage fetch
      const data = window.localStorage;
      yield put(successGet(data));
      yield put(storageInitialized(data));
    }

    
  } catch (e) {
    yield put(failGet(e.name + ': ' + e.message));
  }
}

function* subscribeToStorage() {

  const channel = yield call(eventChannel, (emit) => {
    window.addEventListener('storage', (e) => {
      if (e.key !== storageKey) return;
      
      const wrappedAction = JSON.parse(localStorage.getItem(storageKey));

      if (
        !wrappedAction ||
        !wrappedAction.sourceId ||
        wrappedAction.sourceId === sourceId
      ) return;

      // receive from other tabs
      emit(wrappedAction);

    });
    return () => {};
  });

  while(true) {
    let wrappedAction = yield take(channel);

    if (wrappedAction) {
      yield put(wrappedAction);
    }
  }
}

function* watchStorageInit() {
  while(true) {
    yield take(actionTypes.LOCAL_STORAGE_INIT);
    yield fork(subscribeToStorage);
  }
}

export default [
  fork(watchStorageInit),
  takeLatest(actionTypes.SET_LOCAL_DATA, setLocalDataSaga),
  takeLatest(actionTypes.GET_LOCAL_DATA, getLocalDataSaga)
];

