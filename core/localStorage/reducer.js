import { localStorageActionTypes as actionTypes } from './actions';

export const sourceId = Math.floor(Math.random() * 1000) * Date.now();

export const localStorageExampleInitialState = {
  error: null,
  loading: false,
  data: null
}

function reducer(state = localStorageExampleInitialState, action) {
  switch (action.type) {
    case actionTypes.SET_LOCAL_DATA:
    case actionTypes.GET_LOCAL_DATA:
      return {
        ...state,
        ...{ error: null },
        ...{ loading: true }
      }

    case actionTypes.LOCAL_GET_SUCCESS:
    case actionTypes.LOCAL_SET_SUCCESS:
      return {
        ...state,
        ...{ loading: false },
        ...{ data: { ...state.data, ...action.data } }
      }

    case actionTypes.LOCAL_GET_FAILURE:
    case actionTypes.LOCAL_SET_FAILURE:
      return {
        ...state,
        ...{ loading: false },
        ...{ error: action.error }
      }

    default:
      return state;
  }
}

export default reducer;