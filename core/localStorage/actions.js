export const localStorageActionTypes = {
  SET_LOCAL_DATA: 'SET_LOCAL_DATA',
  GET_LOCAL_DATA: 'GET_LOCAL_DATA',
  LOCAL_SET_SUCCESS: 'LOCAL_SET_SUCCESS',
  LOCAL_GET_SUCCESS: 'LOCAL_GET_SUCCESS',
  LOCAL_SET_FAILURE: 'LOCAL_SET_FAILURE',
  LOCAL_GET_FAILURE: 'LOCAL_GET_FAILURE',
  LOCAL_STORAGE_INIT: 'LOCAL_STORAGE_INITIALIZED'
}

export const failSet = (error) => ({
  type: localStorageActionTypes.LOCAL_SET_FAILURE,
  error
});
export const failGet = (error) => ({
  type: localStorageActionTypes.LOCAL_GET_FAILURE,
  error
});

export const successSet = (data) => ({
  type: localStorageActionTypes.LOCAL_SET_SUCCESS,
  data
});
export const successGet = (data) => ({
  type: localStorageActionTypes.LOCAL_GET_SUCCESS,
  data
});

export const getLocalData = (payload) => ({
  type: localStorageActionTypes.GET_LOCAL_DATA, payload
});
export const setLocalData = (payload) => ({
  type: localStorageActionTypes.SET_LOCAL_DATA, payload
});

export const storageInitialized = () => ({
  type: localStorageActionTypes.LOCAL_STORAGE_INIT
});