export const playerActionTypes = {
  // descriptions
  AUDIO_INITIALIZED: 'AUDIO_INITIALIZED',
  AUDIO_PAUSED: 'AUDIO_PAUSED',
  AUDIO_PLAYING: 'AUDIO_PLAYING',
  AUDIO_READY: 'AUDIO_READY',
  AUDIO_TIME_UPDATED: 'AUDIO_TIME_UPDATED',
  AUDIO_VOLUME_CHANGED: 'AUDIO_VOLUME_CHANGED',

  // imperatives
  PLAY_SELECTED: 'PLAY_SELECTED',
  PAUSE_AUDIO: 'PAUSE_AUDIO',
  AUDIO_SET_VOLUME: 'AUDIO_SET_VOLUME'
};

export const audioInitialized = () => ({
  type: playerActionTypes.AUDIO_INITIALIZED
});
export const audioPaused = () => ({
  type: playerActionTypes.AUDIO_PAUSED
});

export const audioPlaying = () => ({
  type: playerActionTypes.AUDIO_PLAYING
});

export const audioReady = () => ({
  type: playerActionTypes.AUDIO_READY
});

export const audioTimeUpdated = times => ({
  type: playerActionTypes.AUDIO_TIME_UPDATED,
  payload: times
});

export const audioVolumeChanged = volume => ({
  type: playerActionTypes.AUDIO_VOLUME_CHANGED,
  payload: {
    volume
  }
});

export const playSelected = (station, stationListId = null) => ({
  type: playerActionTypes.PLAY_SELECTED,
  payload: {
    station,
    stationListId
  }
});

export const pauseAudio = () => ({
  type: playerActionTypes.PAUSE_AUDIO
});

export const setVolume = volume => ({
  type: playerActionTypes.AUDIO_SET_VOLUME,
  payload: {
    volume
  }
});