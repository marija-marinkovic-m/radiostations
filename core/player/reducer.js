import { SESSION_STATIONLIST_ID } from '../constants';
import { playerActionTypes as actionTypes } from './actions';

export const playerExampleInitialState = {
  isPlaying: false,
  isReady: false,
  stationId: null,
  stationListId: SESSION_STATIONLIST_ID,
  volume: 1,
  initialized: false
};

export default function playerReducer(state = playerExampleInitialState, { payload, type }) {
  switch (type) {

    case actionTypes.AUDIO_INITIALIZED:
      return {
        ...state,
        initialized: true
      }

    case actionTypes.AUDIO_PAUSED:
      return {
        ...state,
        isPlaying: false
      };

    case actionTypes.AUDIO_PLAYING:
      return {
        ...state,
        isPlaying: true
      };

    case actionTypes.AUDIO_READY:
      return {
        ...state,
        isReady: true
      };

    case actionTypes.AUDIO_VOLUME_CHANGED:
      return {
        ...state,
        volume: payload.volume
      };

    case actionTypes.PLAY_SELECTED:
      return {
        ...state,
        isReady: false,
        stationId: payload.station.id,
        stationListId: payload.stationListId || state.stationListId
      };

    default: 
      return state;
  }
}