import * as playerActions from './actions';

export function AudioPlayer(emit) {
  this._audio = new Audio();
  return ({
    initialize: emit => {
      this._audio.preload = "none";
      this._audio.autoplay = false;
    
      this._audio.addEventListener(
        'pause',
        (event) => emit(playerActions.audioPaused())
      );
      this._audio.addEventListener(
        'abort',
        () => emit(playerActions.audioPaused())
      );
      this._audio.addEventListener(
        'playing',
        () => emit(playerActions.audioPlaying())
      );
      this._audio.addEventListener(
        'volumechange',
        () => emit(playerActions.audioVolumeChanged(this._audio.volume))
      );
      this._audio.addEventListener(
        'loadeddata',
        () => emit(playerActions.audioReady())
      );

      return () => {};
    },
    load: url => {
      this._audio.setAttribute('src', url);
    },
    pause: () => {
      this._audio.setAttribute('src', 'about:blank');
      this._audio.pause();
    },
    play: () => {
      let promise = this._audio.play();
      if (promise && promise.catch) promise.catch((e) => console.log('playback error', e));
    },
    setVolume: volume => {
      try {
        const value = parseFloat(volume);
        if (typeof value !== "number") return;
        this._audio.volume = value;
      } catch(e) {
        // error
      }
    }
  });
}