import { PLAYER_STORAGE_KEY } from '../constants';
import { getStationListById, getStationListCursor } from '../stationLists/selectors';
import { getStationById } from '../stations/selectors';

export function getPlayer(state) {
  return state.player;
}

export function getPlayerIsPlaying(state) {
  return state.player.isPlaying;
}

export function getPlayerTimes(state) {
  return state.playerTimes;
}

export function getPlayerStationId(state) {
  return state.player.stationId;
}

export function getPlayerStationListId(state) {
  return state.player.stationListId;
}

export function getPlayerStation(state) {
  const stationId = getPlayerStationId(state);
  return getStationById(stationId, state);
}

export function getPlayerStationList(state) {
  const stationListId = getPlayerStationListId(state);
  return getStationListById(state, stationListId);
}

export function getPlayerStationListCursor(state) {
  const stationId = getPlayerStationId(state);
  const stationList = getPlayerStationList(state);
  return getStationListCursor(stationId, stationList.stationIds);
}

export function getPlayerPrefs(state) {
  const prefsJson = state.localStorage.data && state.localStorage.data[PLAYER_STORAGE_KEY];

  if (!prefsJson) return {volume: 1};

  try {
    return JSON.parse(prefsJson);
  } catch(e) {
    return {volume: 1};
  }
}