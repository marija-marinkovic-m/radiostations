export const stationActionTypes = {
  LOAD_STATION: 'LOAD_STATION',
  FETCH_STATION: 'FETCH_STATION',
  FETCH_STATION_SUCCESS: 'FETCH_STATION_SUCCESS',
  FETCH_STATION_FAILURE: 'FETCH_STATION_FAILURE'
};

export const loadStation = (stationId, data) => ({
  type: stationActionTypes.LOAD_STATION,
  payload: {
    stationId,
    data
  }
});

export const fetchStationPending = stationId => ({
  type: stationActionTypes.FETCH_STATION,
  payload: {
    stationId
  }
});

export const fetchStationSuccess = (stationId, data) => ({
  type: stationActionTypes.FETCH_STATION_SUCCESS,
  payload: {
    stationId,
    data
  }
});

export const fetchStationFail = (stationId, error) => ({
  type: stationActionTypes.FETCH_STATION_FAILURE,
  payload: {
    stationId,
    error
  }
});