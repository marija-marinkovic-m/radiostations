import { stationListActionTypes } from '../stationLists/actions';
import { stationActionTypes as actionTypes } from './actions';

const cdnPfx = '//d35ry4sf1xc0zi.cloudfront.net/images/logos';

export function createStation(data, loading = false, error = null) {
  const { logo, stream, ...other } = data;
  return {
    artworkUrl: logo,
    streamUrl: stream,
    loading,
    error,
    ...other
  };
}

export const stationsExampleInitialState = {};

export default function stationsReducer(state = stationsExampleInitialState, { payload, type }) {

  switch(type) {

    case stationListActionTypes.FETCH_STATIONS_SUCCESS:
      const newStations = payload.collection.reduce((acc, n) => {
        acc[n.id] = createStation(n);
        return acc;
      }, {});
      return {
        ...state,
        ...newStations
      };



    case actionTypes.FETCH_STATION:
      return {
        ...state,
        [payload.stationId]: createStation({id: payload.stationId}, true)
      };
    case actionTypes.LOAD_STATION:
      return {
        ...state,
        [payload.stationId]: payload.data
      }
    case actionTypes.FETCH_STATION_SUCCESS:
      return {
        ...state,
        [payload.stationId]: createStation(payload.data)
      }

    case actionTypes.FETCH_STATION_FAILURE:
      return {
        ...state,
        [payload.stationId]: createStation({id: payload.stationId}, false, payload.error)
      };

    default: 
      return state;
  }
}