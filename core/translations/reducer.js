import { translationsActionTypes as actionTypes } from './actions';

export const translationsExampleInitialState = {
  error: null,
  loading: false,
  translations: null
}

function reducer(state = translationsExampleInitialState, action) {
  switch (action.type) {

    case actionTypes.SET_TRANSLATION:
      return {
        ...state,
        error: null,
        loading: true 
      }

    case actionTypes.SET_TRANSLATION_SUCCESS:
      return {
        ...state,
        loading: false,
        translations: { ...state.translations, ...action.data }
      }

    case actionTypes.SET_TRANSLATION_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.error
      }

    default:
      return state;

  }
}

export default reducer;