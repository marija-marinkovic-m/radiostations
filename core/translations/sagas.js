import { put, takeLatest } from 'redux-saga/effects';

import { translationsActionTypes as actionTypes, translationsSuccess, translationsFailure } from './actions';
import { getTranslation } from '../../i18n/translationHelpers';

function* setTranslationSaga({ lang, namespaces }) {
  try {
    const data = yield getTranslation(lang, namespaces);
    yield put(translationsSuccess(data));
  } catch (e) {
    yield put(translationsFailure(`${e.name}: ${e.message}`));
  }
}

export default [
  takeLatest(actionTypes.SET_TRANSLATION, setTranslationSaga)
];