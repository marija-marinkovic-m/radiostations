export const STATIONS_PER_PAGE = 4;
export const SESSION_STATIONLIST_ID = 'session-stationlist-id';

export const PLAYER_STORAGE_KEY = process.env.PLAYER_STORAGE_KEY || 'playerPrefs';
export const FAVORITES_STORAGE_KEY = process.env.FAVORITES_STORAGE_KEY || 'favorites';
export const HISTORY_STORAGE_KEY = process.env.HISTORY_STORAGE_KEY || 'history';
export const PLAYER_QUERY_VAR = process.env.PLAYER_QUERY_VAR || 'player';

export const STORAGE_STATIONS_LIMIT = parseInt(process.env.STORAGE_STATIONS_LIMIT, 10) || 10;
