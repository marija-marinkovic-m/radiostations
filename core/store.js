import { createStore, applyMiddleware } from 'redux';
import withRedux from 'next-redux-wrapper';
import nextReduxSaga from 'next-redux-saga';
import createSagaMiddleware from 'redux-saga';

import rootReducer, { exampleInitialState } from './reducers';
import rootSaga from './sagas';

import { createStorageMiddleware } from './sync-middleware';

const sagaMiddleware = createSagaMiddleware();
const storageMiddleware = createStorageMiddleware();

const bindMiddleware = (middleware) => {
  if (process.env.NODE_ENV !== 'production') {
    const { composeWithDevTools } = require('redux-devtools-extension')
    return composeWithDevTools(applyMiddleware(...middleware))
  }
  return applyMiddleware(...middleware)
}

export function configureStore(initialState = exampleInitialState) {
  const store = createStore(
    rootReducer,
    initialState,
    bindMiddleware([sagaMiddleware, storageMiddleware])
  );

  store.sagaTask = sagaMiddleware.run(rootSaga)
  return store
}

export function withReduxSaga(BaseComponent, mapState) {
  return withRedux(configureStore, mapState)(nextReduxSaga(BaseComponent))
}
