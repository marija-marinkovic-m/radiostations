import { cookiesActionTypes as actionTypes } from './actions';

export const cookiesExampleInitialState = {
  error: null,
  loading: false,
  theme: 'light',
  lang: 'sr'
};

function reducer(state = cookiesExampleInitialState, action) {
  switch (action.type) {

    case actionTypes.SET_COOKIE:
    case actionTypes.GET_COOKIE:
      return {
        ...state,
        ...{ error: null },
        ...{ loading: true }
      };

    case actionTypes.COOKIE_SUCCESS:
      return {
        ...state,
        ...{ loading: false },
        ...{ [action.key]: action.value }
      };

    case actionTypes.COOKIE_FAILURE:
      return {
        ...state,
        ...{ loading: false },
        ...{ error: action.error }
      }

    default:
      return state;
  }
}

export default reducer;