export const cookiesActionTypes = {
  SET_COOKIE: 'SET_COOKIE',
  GET_COOKIE: 'GET_COOKIE',
  COOKIE_SUCCESS: 'COOKIE_SUCCESS',
  COOKIE_FAILURE: 'COOKIE_FAILURE'
};

/** 
 * @key
 * @value
 * @days (until expire)
 * */
export const setCookie = (ctx = null, key = 'theme', value = 'light', days = 30) => ({
  type: cookiesActionTypes.SET_COOKIE,
  ctx, key, value, days
});

/**
 * @ctx : next react component context (from getInitialProps)
 * @key: cookie name (string)
 * @defValue: default cookie value
 * */
export const getCookie = (ctx, key = 'theme', defValue = 'light') => ({
  type: cookiesActionTypes.GET_COOKIE,
  ctx, key, defValue
});

/**
 * @key
 * @value: string (value ie. 'light', 'dark',...)
 */
export const cookieSuccess = (key = 'theme', value = 'light') => ({
  type: cookiesActionTypes.COOKIE_SUCCESS,
  key, value
});

/**
 * @msg: string
 */
export const cookieFailure = (error) => ({
  type: cookiesActionTypes.COOKIE_FAILURE,
  error
});