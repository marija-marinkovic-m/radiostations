import { combineReducers } from 'redux';

import localReducer, { localStorageExampleInitialState } from './localStorage/reducer';
import cookieReducer, { cookiesExampleInitialState } from './cookies/reducer';
import translationReducer, { translationsExampleInitialState } from './translations/reducer';

import stationListsReducer, { stationListsExampleInitialState } from './stationLists/reducer';
import stationsReducer, { stationsExampleInitialState } from './stations/reducer';
import {
  resourcesReducer, resourcesExampleInitialState,
  sidebarNavReducer, sidebarNavInitialState
} from './api/reducer';

import playerReducer, { playerExampleInitialState } from './player/reducer';

export const exampleInitialState = {
  localStorage: localStorageExampleInitialState,
  cookies: cookiesExampleInitialState,
  translations: translationsExampleInitialState,
  stationLists: stationListsExampleInitialState,
  stations: stationsExampleInitialState,
  apiResources: resourcesExampleInitialState,
  sidebarNav: sidebarNavInitialState,
  player: playerExampleInitialState
};

const rootReducer = combineReducers({
  localStorage: localReducer,
  cookies: cookieReducer,
  translations: translationReducer,
  stationLists: stationListsReducer,
  stations: stationsReducer,
  apiResources: resourcesReducer,
  sidebarNav: sidebarNavReducer,
  player: playerReducer
});

export default rootReducer;