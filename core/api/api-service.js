import * as contentful from 'contentful';

const SPACE_ID = 'jyuc0vqp30bn';
const ACCESS_TOKEN = 'f2d0f260b09b178b49279a46e215703871ff521f42efb0851d6010cf5389c800';

const client = contentful.createClient({
  space: SPACE_ID,
  accessToken: ACCESS_TOKEN
});

const entryTransform = (entry) => {
  const { sys, fields:{ title, stream, logo, category, ...other } } = entry;
  return {
    id: sys.id,
    title,
    stream,
    logo: logo && logo.fields && logo.fields.file && logo.fields.file.url,
    categories: category ? category.map(c => c.sys.id) : [],
    ...other
  };
};

export const api = {
  fetchStations(params = {}) {
    return client.getEntries({
      content_type: 'radioStation',
      include: 2,
      ...params
    })
    .then(response => {
      const includes = response.includes;
      const items = response.items.map(entryTransform);
      return {
        collection: items.map(i => {
          return Object.assign({}, i, {
            categories: i.categories.map(id => {
              const cat = includes.Entry.filter(e => e.sys.id === id);

              if (cat.length) {
                return {
                  ...cat[0].fields,
                  id
                }
              }

              return {id}
            })
          });
        }),
        pagination: false,
        urlParams: params
      };
    })
    .catch(error => console.log('Error occured while fetching entries for radioStation', error));
  },
  fetchStation(stationId, params = {}) {
    return client.getEntries({
      content_type: 'radioStation',
      'sys.id': stationId,
      ...params
    })
    .then(response => {
      const includes = response.includes;
      const data = response.items.map(entryTransform)[0];

      return Object.assign({}, data, {
        categories: data.categories.map(id => {
          const cat = includes.Entry.filter(e => e.sys.id === id);

          if (cat.length) {
            return {
              ...cat[0].fields,
              id
            }
          }

          return {id}
        })
      });
    })
    .catch(error => console.log('Error occured while fetching single station entry', error));
  },
  fetchResources() {
    return new Promise((res, rej) => res({}));
  }
}