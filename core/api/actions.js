export const actions = {
  FETCH: '@apiRes/FETCH',
  FETCH_PENDING: '@apiRes/FETCH_PENDING',
  FETCH_SUCCESS: '@apiRes/FETCH_SUCCESS',
  FETCH_FAIL: '@apiRes/FETCH_FAIL',
  LOAD: '@apiRes/LOAD_FROM_STATE',

  // stations
  GET_STATIONS: '@apiStations/GET_STATIONS',
  GET_STATION: '@apiStations/GET_STATION',

  // navigation
  SET_NAV: '@apiNav/SET_NAV',
  NAV_PENDING: '@apiNav/NAV_PENDING',
  NAV_SUCCESS: '@apiNav/NAV_SUCCESS',
  NAV_FAIL: '@apiNav/NAV_FAIL'
};

export const navLinkNormalizer = (id, title, pathname, icon = null, nextQuery = {}) => ({
  id,
  title,
  pathname,
  icon,
  nextQuery
});

export const setNav = (nationalitySlug) => ({
  type: actions.SET_NAV,
  payload: {
    nationalitySlug
  }
});
export const setNavPending = (nationalitySlug) => ({
  type: actions.NAV_PENDING,
  payload: {
    nationalitySlug
  }
});
export const setNavSuccess = (nationalitySlug, links) => ({
  type: actions.NAV_SUCCESS,
  payload: {
    links,
    nationalitySlug
  }
});
export const setNavFail = (nationalitySlug, error) => ({
  type: actions.NAV_FAIL,
  payload: {
    error,
    nationalitySlug
  }
});

export const getStations = (stationListId, params) => ({
  type: actions.GET_STATIONS,
  payload: {
    stationListId,
    params
  }
});

export const getStation = (stationId, params = {}) => ({
  type: actions.GET_STATION,
  payload: {
    stationId,
    params
  }
});

export const fetchApiResource = (resourceType, params = {}) => ({
  type: actions.FETCH,
  payload: {
    resourceType,
    params
  }
});

export const fetchApiResourcePending = (resourceType) => ({
  type: actions.FETCH_PENDING,
  payload: {
    resourceType
  }
});

export const fetchApiResourceSuccess = (resourceType, response) => ({
  type: actions.FETCH_SUCCESS,
  payload: {
    resourceType,
    response
  }
});

export const fetchApiResourceFail = (error, resourceType) => ({
  type: actions.FETCH_FAIL,
  payload: {
    resourceType,
    error
  }
});

export const loadApiResource = (resourceType, response) => ({
  type: actions.LOAD,
  payload: {
    resourceType,
    response
  }
});