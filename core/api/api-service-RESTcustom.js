/** global fetch */
import qs from 'query-string';
import { STATIONS_PER_PAGE } from '../constants';

import es6promise from 'es6-promise';
import 'isomorphic-unfetch';
es6promise.polyfill();

const defaultStationListParams = {
  perPage: STATIONS_PER_PAGE,
  include: 'stream,details',
  page: 1
};

const defaultStationParams = {
  include: 'stream,details'
};

export const api = {
  fetchStations: (params = {}) => {
    const urlParams = Object.assign({}, defaultStationListParams, params);
    return fetch(requestUrl('/stations', urlParams))
      .then(response => response.json())
      .then(json => ({
        collection: json.data,
        pagination: json.paginator,
        urlParams
      }));
  },
  fetchStation: (stationId, params = {}) => {
    const urlParams = Object.assign({}, defaultStationParams, params);
    return fetch(requestUrl(`/stations/${stationId}`, urlParams))
      .then(response => response.json())
      .then(json => json.data);
  },
  fetchResources: (resource, params) => {
    return fetch(requestUrl(`/${resource}`, params))
      .then(response => response.json());
  }
};

function requestUrl (endpoint, query = null, apiUrl = process.env.API_URL) {
  const params = query ? '?' + qs.stringify(query) : '';
  return apiUrl + endpoint + params;
}