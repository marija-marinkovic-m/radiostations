import { call, put, fork, takeEvery, select } from 'redux-saga/effects';
import { api } from './api-service';
import {
  actions,
  getStations,
  fetchApiResourceFail, fetchApiResourceSuccess, fetchApiResourcePending,
  loadApiResource,
  setNavSuccess, fetchApiResource,
  navLinkNormalizer as navLink
} from './actions';

import {
  fetchStationsFail, fetchStationsSuccess, fetchStationsPending,
  loadStationList
} from '../stationLists/actions';

import {
  fetchStationSuccess, fetchStationFail, fetchStationPending,
  loadStation
} from '../stations/actions';

import { getStationListById } from '../stationLists/selectors';
import { getStationById } from '../stations/selectors';
import { getResource, isSetSidebarNav } from './selectors';

// SAGAS
export function* fetchStationsListSaga(action) {
  const { payload:{ stationListId, params } } = action;
  const stationList = yield select(getStationListById.bind(null, stationListId));
  if (stationList && params && stationList.currentPage >= params.page) {
    yield put(loadStationList(stationListId));
  } else {
    yield call(fetchStations, stationListId, params);
  }
}
export function* fetchStationSaga(action) {
  const { payload: {stationId, params} } = action;
  const station = yield select(getStationById.bind(null, stationId));
  if (station !== false) {
    yield put(loadStation(stationId, station));
  } else {
    yield call(fetchStation, stationId, stationId, params);
  }
}
function* fetchResourceSaga({type, payload: { resourceType, params }}) {
  const stateResource = yield select(getResource.bind(null, resourceType));
  if (stateResource !== false) {
    yield put(loadApiResource(resourceType, stateResource));
  } else {
    yield call(fetchResources, resourceType, resourceType, params)
  }
}

function* setSidebarNavSaga({type, payload: { nationalitySlug }}) {
  const stateSidebarNav = yield select(isSetSidebarNav);
  if (stateSidebarNav !== false && nationalitySlug === stateSidebarNav.nationalitySlug) {
    yield put(setNavSuccess(nationalitySlug, stateSidebarNav.links));
  } else {

    const links = {
      main: [
        navLink('home', 'home', '/', 'home'),
        navLink('popular', 'most popular', `/${nationalitySlug}/radio-stanice`, 'list'),
        navLink('trending', 'trending', `/${nationalitySlug}/uzivo`, 'trending_up')
      ]
    };

    yield put(setNavSuccess(nationalitySlug, links));
    yield put(fetchApiResource('genres', {
      perPage: 8
    }));
    yield put(fetchApiResource('cities', {
      nationality_slug: nationalitySlug,
      perPage: 8
    }));
  }
}

// HELPERS
/**
 * 
 * @param {fn} apiFn 
 * @param {object: {pending: fn, success: fn, fail: fn}} actions 
 */
function* fetchEntities(apiFn, actions, id, ...apiFnArgs) {
  try {
    yield put(actions.pending(id));
    const data = yield call(apiFn, ...apiFnArgs);
    yield put(actions.success(id, data));
  } catch (error) {
    yield put(actions.fail(error, id));
  }
}
// fetchEntities expect id; fetchStations expect params obj
export const fetchStations = fetchEntities.bind(null, api.fetchStations, {
  pending: fetchStationsPending,
  success: fetchStationsSuccess,
  fail: fetchStationsFail
});
export const fetchStation = fetchEntities.bind(null, api.fetchStation, {
  pending: fetchStationPending,
  success: fetchStationSuccess,
  fail: fetchStationFail
});
export const fetchResources = fetchEntities.bind(null, api.fetchResources, {
  pending: fetchApiResourcePending,
  success: fetchApiResourceSuccess,
  fail: fetchApiResourceFail
})

// ROOT
export default [
  takeEvery(actions.GET_STATIONS, fetchStationsListSaga),
  takeEvery(actions.GET_STATION, fetchStationSaga),
  takeEvery(actions.FETCH, fetchResourceSaga),
  takeEvery(actions.SET_NAV, setSidebarNavSaga)
];