import { actions as actionTypes, navLinkNormalizer as navLink } from './actions';

const resources = [
  'countries',
  'genres',
  'cities',
];
const navResources = [
  'cities',
  'genres'
];

export const resourcesExampleInitialState = resources.reduce((acc, n) => {
  acc[n] = {loading: false, response: {data: []}, error: null, isNew: true};
  return acc;
}, {});

export const sidebarNavInitialState = {
  pending: false,
  error: null,
  links: {
    main: [],
    locations: [],
    genres: []
  },
  nationalitySlug: null
};

export function resourcesReducer(state = resourcesExampleInitialState, { type, payload = {resourceType: '', response: {data: []}, error: null} }) {
  const { resourceType, response, error } = payload;

  switch(type) {
    case actionTypes.FETCH_PENDING:
      return {
        ...state,
        [resourceType]: Object.assign({}, state[resourceType], {loading: true, error: null, isNew: false})
      };
    case actionTypes.FETCH_SUCCESS:
    case actionTypes.LOAD:
      return {
        ...state,
        [resourceType]: Object.assign({}, state[resourceType], {loading: false, response, error: null})
      };
    case actionTypes.FETCH_FAIL:
      return {
        ...state,
        [resourceType]: Object.assign({}, state[resourceType], {loading: false, error})
      };
    default:
      return state;
  }
}

export function sidebarNavReducer(state = sidebarNavInitialState, { type, payload }) {
  // payload shape { nationalitySlug: '', links: {main: [], locations: [], genres: []}, error: null }
  switch(type) {
    case actionTypes.NAV_PENDING:
      return {
        ...state,
        pending: true,
        error: null,
        nationalitySlug: payload.nationalitySlug
      };
    case actionTypes.NAV_SUCCESS:
      return {
        ...state,
        pending: false,
        links: payload.links,
        nationalitySlug: payload.nationalitySlug
      };
    case actionTypes.NAV_FAIL:
      return {
        ...state,
        pending: false,
        error: payload.error,
        nationalitySlug: payload.nationalitySlug
      };

    case actionTypes.FETCH_PENDING:
      if (navResources.indexOf(payload.resourceType) < 0) return state;
      return {
        ...state,
        pending: true
      };
    case actionTypes.FETCH_FAIL:
      if (navResources.indexOf(payload.resourceType) < 0) return state;
      return {
        ...state,
        pending: false,
        error: payload.error
      }
    case actionTypes.FETCH_SUCCESS:
      console.log('fetch_success from nav reducer', payload);
      if (navResources.indexOf(payload.resourceType) < 0) return state;
      
      const navResource = payload.resourceType === 'cities' ? 'locations' : payload.resourceType;
      return {
        ...state,
        pending: false,
        links: {
          ...state.links,
          [navResource]: payload.response.data.map(i => {
            const getParam = navResource === 'locations' ? 'lokacija' : 'zanr';
            const valueParam = navResource === 'locations' ? 'name' : 'slug';
            const pathname = `/${state.nationalitySlug}/radio-stanice`;
            const nextQuery = {[getParam]: i[valueParam]};

            return navLink(i.id, i.name, pathname, null, nextQuery);
          })
        }
      };
    default: 
      return state;
  }
}