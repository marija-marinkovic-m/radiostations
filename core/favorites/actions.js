export const favoritesActionTypes = {
  TOGGLE_FAVORITE: 'TOGGLE_FAVORITE'
};

export const toggleFavorite = station => ({
  type: favoritesActionTypes.TOGGLE_FAVORITE,
  payload: {
    station
  }
});