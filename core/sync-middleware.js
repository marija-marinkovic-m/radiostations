import { playerActionTypes } from './player/actions';
import { localStorageActionTypes } from './localStorage/actions';
import { translationsActionTypes } from './translations/actions';


export const sourceId = Math.floor(Math.random() * 1000) * Date.now();
export const storageKey = 'radiostanica-sync-store';


const excludedActions = ((...args) => {
  return args
    .map((types) => Object.keys(types).map(a => types[a]))
    .reduce((acc, n) => ([...acc, ...n]), []);
})(
  playerActionTypes,
  localStorageActionTypes,
  translationsActionTypes
);

function wrapAction(action) {
  return {
    ...action,
    sourceId
  }
}

export function createStorageMiddleware() {
  return store => next => action => {

    if (typeof window === 'undefined')
      return next(action);
    
    if (excludedActions.indexOf(action.type) > -1)
      return next(action);

    if (action.sourceId && action.sourceId !== sourceId)
      return next(action);

    // send to other tabs
    const wrappedAction = wrapAction(action);
    localStorage.setItem(
      storageKey,
      JSON.stringify(wrappedAction)
    )

    next(action);
  }
}
