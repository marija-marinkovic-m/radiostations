export const historyActionTypes = {
  ADD_HISTORY: 'ADD_HISTORY'
};

export const addHistory = station => ({
  type: historyActionTypes.ADD_HISTORY,
  payload: {
    station
  }
});