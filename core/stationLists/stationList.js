import { STATIONS_PER_PAGE } from '../constants';
import { stationListActionTypes as actionTypes } from './actions';

export const StationList = {
  currentPage: 0,
  hasNextPage: false,
  id: null,
  isNew: true,
  isPending: false,
  nextUrlParams: null,
  stationIds: []
};

export function stationListReducer(state = StationList, { payload, type }) {
  switch(type) {
    case actionTypes.FETCH_STATIONS_SUCCESS:
      const stationIds = mergeStationIds(state.stationIds, payload.collection);
      const paginationProps = updatePagination(
        state,
        payload.pagination,
        payload.urlParams
      );
      const updatedState = {
        id: payload.stationListId,
        isNew: false,
        isPending: false,
        stationIds,
        ...paginationProps
      }
      return {
        ...state,
        ...updatedState
      };

    case actionTypes.FETCH_STATIONS_PENDING:
      return {
        ...state,
        isPending: true
      };

    default: 
      return state;
  }
}

function mergeStationIds(stationIds, collection) {
  let newIds = collection.reduce((list, stationData) => {
    if (stationIds.indexOf(stationData.id) === -1) list.push(stationData.id);
    return list;
  }, []);

  return newIds.length ? stationIds.concat(newIds) : stationIds;
}

/**
 * 
 * @param {*} stationList 
 * @param {*} pagination number || object shape({total: number, count: number, per_page: number, current_page: number, total_pages: number, links: {next: string, previous: string}})
 * @param {*} urlParams
 */
function updatePagination(stationList, pagination, urlParams = {}) {
  let { hasNextPage, nextUrlParams, currentPage } = stationList;

  if (typeof pagination === 'object' && currentPage <= pagination.current_page) {
    currentPage = pagination.current_page;
    hasNextPage = Boolean(pagination.links.next);
    nextUrlParams = Object.assign(urlParams, {page: currentPage+1});
  } else {
    currentPage = Math.ceil(stationList.stationIds.length / STATIONS_PER_PAGE);
  }

  return {
    currentPage,
    hasNextPage,
    nextUrlParams
  };
}