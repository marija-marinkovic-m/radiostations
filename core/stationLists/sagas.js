import { call, fork, put, select, takeEvery } from 'redux-saga/effects';
import { stationListActionTypes as actionTypes, updatePagination } from './actions';

import { getStations } from '../api/actions';
import { getCurrentStationList } from './selectors';

export function * loadNextStations() {
  const stationList = yield select(getCurrentStationList);
  console.log('currentStationList', stationList);
  if (stationList.nextUrlParams) {
    console.log('has next params')
    console.log('next params', stationList.nextUrlParams);
    yield put(getStations(stationList.id, stationList.nextUrlParams));
  }
}

export default [
  takeEvery(actionTypes.LOAD_NEXT_STATIONS, loadNextStations)
];