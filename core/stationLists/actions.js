export const stationListActionTypes = {
  LOAD_STATIONLIST: 'LOAD_STATIONLIST',
  LOAD_NEXT_STATIONS: 'LOAD_NEXT_STATIONS',

  FETCH_STATIONS_FAILURE: 'FETCH_STATIONS_FAILURE',
  FETCH_STATIONS_SUCCESS: 'FETCH_STATIONS_SUCCESS',
  FETCH_STATIONS_PENDING: 'FETCH_STATIONS_PENDING'
};

export const loadStationList = stationListId => ({
  type: stationListActionTypes.LOAD_STATIONLIST,
  payload: {
    stationListId
  }
});

export const loadNextStations = () => ({
  type: stationListActionTypes.LOAD_NEXT_STATIONS
});

export const fetchStationsFail = error => ({
  type: stationListActionTypes.FETCH_STATIONS_FAILURE,
  payload: error
});
export const fetchStationsSuccess = (stationListId, data) => ({
  type: stationListActionTypes.FETCH_STATIONS_SUCCESS,
  payload: {
    stationListId,
    ...data
  }
});
export const fetchStationsPending = stationListId => ({
  type: stationListActionTypes.FETCH_STATIONS_PENDING,
  payload: {
    stationListId
  }
});
