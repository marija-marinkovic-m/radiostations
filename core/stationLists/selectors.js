import { createSelector } from 'reselect';
import { STATIONS_PER_PAGE } from '../constants';
import { getStations } from '../stations/selectors';

export function getStationLists(state) {
  return state.stationLists;
}

export function getStationListById(stationListId, state) {
  const stationLists = getStationLists(state);
  return stationLists && stationLists[stationListId];
}

export function getCurrentStationList(state) {
  let stationLists = getStationLists(state);
  return stationLists[stationLists.currentStationListId];
}

export function getStationListCursor(selectedStationId, stationIds) {
  let index = stationIds.indexOf(selectedStationId);
  let nextStationId = null;
  let previousStationId = null;

  if (index !== -1) {
    if (index < stationIds.size - 1) nextStationId = stationIds.get(index + 1);
    if (index > 0) previousStationId = stationIds.get(index - 1);
  }

  return {
    nextStationId,
    previousStationId,
    selectedStationId
  }
}


// memoized selectors
export const getCurrentPage = createSelector(
  getCurrentStationList,
  stationList => stationList.currentPage
);

export const getCurrentStationIds = createSelector(
  getCurrentStationList,
  stationList => stationList.stationIds
);

export const getStationsForCurrentStationList = createSelector(
  getCurrentPage,
  getCurrentStationIds,
  getStations,
  (currentPage, stationIds, stations) => {
    return stationIds
      .slice(0, currentPage * STATIONS_PER_PAGE)
      .map(id => stations[id]);
  }
);

