import { PLAYER_QUERY_VAR } from '../core/constants';

/**
 * getLinkHref util
 * @param {query} prevQuery current router query object
 * @param {nextPathname} nextPathname link to page pathname
 * @param {nextQuery} nextQuery link to page query
 */
function getLinkHref(prevQuery, nextPathname = '/', nextQuery = {}) {
  const query = prevQuery && prevQuery[PLAYER_QUERY_VAR] ?
    Object.assign({}, { [PLAYER_QUERY_VAR]: 1 }, nextQuery) :
    nextQuery;
  const pathname = nextPathname;


  console.log('result from getLinkHref', {pathname, query});
  return {
    pathname,
    query
  }
}

export default getLinkHref;