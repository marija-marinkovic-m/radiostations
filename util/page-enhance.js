import React from 'react';
import { I18nextProvider } from 'react-i18next';
import startI18n from '../i18n/startI18n';
import { withReduxSaga } from '../core/store';

import { getLocalData } from '../core/localStorage/actions';
import { getCookie } from '../core/cookies/actions';
import { setTranslation } from '../core/translations/actions';

const defaultLang = 'en';
const defaultTheme = 'light';

function pageEnhance(Component, stateToProps = () => ({})) {

  const mapStateToProps = store => ({
    ...stateToProps(store),
    lang: store.cookies.lang,
    translations: store.translations.translations,
    localStorage: store.localStorage.data
  });

  class PageEnhance extends React.Component {
    state = {
      i18n: null
    }

    static async getInitialProps(ctx) {
      if (ctx && ctx.res) {
        // server req, parse cookies
        await ctx.store.dispatch(getCookie(ctx, 'theme', defaultTheme));
        await ctx.store.dispatch(getCookie(ctx, 'lang', defaultLang));
      }

      if (Component.getInitialProps) {
        return Component.getInitialProps(ctx);
      }

      return {};
    }

    componentWillMount() {
      this.initializeI18n(this.props);

      // initialize localStorage due to storage availability
      if (!this.props.localStorage) {
        this.props.dispatch(getLocalData());
      }
    }

    componentWillReceiveProps(nextProps) {
      if (nextProps.lang !== this.props.lang) {
        this.changeLanguageI18n(nextProps);
      }

      if (Object.keys(nextProps.translations).length !== Object.keys(this.props.translations)) {
        // translations resources updated
        // reinit i18n with new data
        this.initializeI18n(nextProps);
      }
    }

    changeLanguageI18n = (props) => {
      const { i18n } = this.state;

      if (!i18n 
        || !i18n.languages
        || i18n.languages.indexOf(props.lang) < 0) {
        // i18n not initialized
        // or lacks lang resources
        return props.dispatch(setTranslation(props.lang));
      }

      i18n.changeLanguage(props.lang);
    }

    initializeI18n = (props) => {
      this.setState({ i18n: startI18n(props.translations, props.lang) });
    }

    render() {
      return(
        <I18nextProvider i18n={this.state.i18n}>
          <Component {...this.props} />
        </I18nextProvider>
      );
    }
  }

  return withReduxSaga(PageEnhance, mapStateToProps);
}

export default pageEnhance;