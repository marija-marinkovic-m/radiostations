import React from 'react';
import pageEnhance from '../util/page-enhance';

import Typography from 'material-ui/Typography';

import Layout from '../components/layout';
import Head from '../components/head';
import StationItem from '../components/station-item';
import { NavBar, ActionBar } from '../components/single-station-elements';
import StationGrid from '../components/station-grid';
import TitleBar from '../components/misc/title-bar';
import ImgTag from '../components/ImgTag';
import Grid from 'material-ui/Grid';

import { getStationById, getStations } from '../core/stations/selectors';
import { getStationLists } from '../core/stationLists/selectors';

import { getStation, getStations as getStationsAction } from '../core/api/actions';

import { api } from '../core/api/api-service'

const metaTags = {
  title: '',
  description: '',
  url: '',
  ogImage: ''
};

class SingleStation extends React.PureComponent {

  state = {
    catID: null
  }

  static async getInitialProps({store, query}) {

    const stationId = query.id;
    await store.dispatch(getStation(stationId, {}));
    return {
      stationId
    };
  }

  componentDidMount() {
    const { stationId, stations } = this.props;
    const station = stations[stationId];

    if (station && station.categories.length) {
      const cat = station.categories[0].id;
      this.props.dispatch(getStationsAction(`${cat}-list`, {
        'fields.category.sys.id': cat,
        'sys.id[nin]': stationId
      }));
      this.setState({catID: cat});
    }
  }

  renderError = (error) => {
    return <code>{ JSON.stringify(error) }</code>
  }
  renderLoader = () => (<div>loading...</div>)

  renderStation = ({station}) => {
    if (!station) return null;
    if (station.loading) return this.renderLoader();
    if (null !== station.error) return this.renderError(station.error);

    return (<div>
      {/* <NavBar /> */}
      <StationItem
        station={station}
        size="lg">
        <div><ActionBar station={station} /></div>
      </StationItem>
    </div>);
  }

  renderRelated = (stationListId, stationLists, stations) => {
    if (!stationLists || !stationLists[stationListId]) return null;

    const relatedStations = stationLists[stationListId].stationIds.map(stationId => stations[stationId]);

    return relatedStations.length ? (
      <div style={{ marginBottom: 55 }}>
        <TitleBar title="related" titleComponent="h4" titleVariant="title" />
        <StationGrid size="sm" zoom={false} stations={relatedStations} />
      </div>
    ) : null;
  }

  render() {
    const { stations, stationId, stationsList } = this.props;
    const station = stations[stationId];
    const StationCard = this.renderStation;
    const Related = this.state.catID ? this.renderRelated.bind(null, `${this.state.catID}-list`, stationsList, stations) : () => null;
    return (
      <Layout>
        <Head title="Single Station" />

        <Grid container spacing={40} justify="space-between">
          <Grid item md={8} xs={12}>
            <StationCard station={station} />

            <div style={{marginBottom: 60}} />

            <Related />

          
          </Grid>
          <Grid item md={4} xs={12}>
            <ImgTag
              src="/static/temp/300x600.jpg"
              render={(vs) => <img src={vs} alt="banner" style={{marginBottom: 40, cursor: 'pointer', width: '100%'}} />} />
          </Grid>
        </Grid>

      </Layout>
    );
  }
}

export default pageEnhance(SingleStation, 
  state => ({
    stations: getStations(state),
    stationsList: getStationLists(state)
  })
);