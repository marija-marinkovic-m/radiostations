import React from 'react';

import Head from '../components/head';
import Layout from '../components/layout';

import Button from 'material-ui/Button';
import Typography from 'material-ui/Typography';
import Grid from 'material-ui/Grid';

import pageEnhance from '../util/page-enhance';
import StationItem from '../components/station-item';
import ImgTag from '../components/ImgTag';

import { getStations } from '../core/api/actions';
import { loadNextStations } from '../core/stationLists/actions';
import { getStationLists } from '../core/stationLists/selectors';

class Popular extends React.Component {

  static async getInitialProps({store, query}) {
    const stationListId = `${query.cat}-list`;
    const stationListParams = {
      'fields.category.sys.id': query.cat
    };

    await store.dispatch(getStations(stationListId, stationListParams));

    return {
      stationListId
    }
  }

  loadMore = (e) => {
    e.preventDefault();
    this.props.dispatch(loadNextStations());
  }

  render() {

    const { stations, lists, stationListId } = this.props;
    const list = lists && lists[stationListId];
    return (
      <Layout>
        <Head title="Popular" />
        <Grid container spacing={40} justify="space-between">
          <Grid item md={8} xs={12}>
            { list && list.stationIds.map((id) => <StationItem key={id} station={stations[id]} />) }


            { list && list.hasNextPage && <button onClick={this.loadMore}>Load More</button> }
          </Grid>
          <Grid item md={4} xs={12}>
            <ImgTag
              src="/static/temp/300x600.png"
              render={(vs) => <img src={vs} alt="banner" style={{marginBottom: 40, cursor: 'pointer', width: '100%'}} />} />
          </Grid>
        </Grid>


      </Layout>
    );
  }
}

export default pageEnhance(Popular,
  (state) => ({
    stations: state.stations,
    lists: getStationLists(state)
  })
);