import React from 'react';

import Head from '../components/head';
import Layout from '../components/layout';

import Button from 'material-ui/Button';
import Typography from 'material-ui/Typography';

import pageEnhance from '../util/page-enhance';
import StationItem from '../components/station-item';

import { getStations } from '../core/api/actions';
import { loadNextStations } from '../core/stationLists/actions';
import { getStationLists } from '../core/stationLists/selectors';


const stationListTemplate = {
  stationListId: '%-popular',
  params: {
    nationalitites: '%',
    order: 'popular'
  }
}

class Popular extends React.Component {

  static async getInitialProps({store, query}) {
    /**
     * @todo need to provide some front screen for user to select 'nationality' if no nationalitySlug provided
     */
    const nationalitySlug = query.nationalitySlug || 'srbija';
    let stationListId = stationListTemplate.stationListId.replace(/%/g, nationalitySlug);
    let stationListParams = {
      nationalitites: nationalitySlug
    };

    // check for other query params
    // for now `genre` and `location`
    if (query.genre) {
      stationListId = `${stationListId}-${query.genre}`;
      stationListParams.genres = query.genre;
    }
    if (query.location) {
      stationListId = `${stationListId}-${query.location}`;
      stationListParams.locations = query.location;
    }

    await store.dispatch(getStations(stationListId, stationListParams));

    return {
      stationListId
    }
  }

  loadMore = (e) => {
    e.preventDefault();
    this.props.dispatch(loadNextStations());
  }

  render() {

    const { stations, lists, stationListId } = this.props;
    const list = lists && lists[stationListId];
    return (
      <Layout>
        <Head title="Popular" />


        <div className="clearfix">
          <section className="content-section">
            { list && list.stationIds.map((id) => <StationItem key={id} station={stations[id]} />) }


            { list && list.hasNextPage && <button onClick={this.loadMore}>Load More</button> }
          </section>
          <aside className="content-aside">
            <img src="/static/temp/300x600.png" alt="banner" />
          </aside>
        </div>


      </Layout>
    );
  }
}

export default pageEnhance(Popular,
  (state) => ({
    stations: state.stations,
    lists: getStationLists(state)
  })
);