import React from 'react';

import Link from 'next/link';
import Head from '../components/head';
import { setLocalData, getLocalData } from '../core/localStorage/actions';

import pageEnhance from '../util/page-enhance';

import Layout from '../components/layout';
import Button from 'material-ui/Button';
import TextField from 'material-ui/TextField';
import Typography from 'material-ui/Typography';

import TitleBar from '../components/misc/title-bar';
import StationGrid from '../components/station-grid';

import { getStations as getStationsAction } from '../core/api/actions';

import { getStations } from '../core/stations/selectors';
import { getStationLists } from '../core/stationLists/selectors';
import ImgTag from '../components/ImgTag';

class HomePage extends React.Component {

  static async getInitialProps({store, query}) {
    // list id
    const id = 'index-list';
    await store.dispatch(getStationsAction(id));
    return {
      widgets: [{id}]
    }
  }

  renderStationList = ({id, list}) => {
    const { stations } = this.props;
    const currentStations = list.stationIds
      .slice(0,6)
      .map(stationId => stations[stationId]);

    return (
      <div style={{marginBottom: 55}}>
        <TitleBar title={id} link="#link" />
        <StationGrid stations={currentStations} />
      </div>
    );
  }

  render () {
    const { widgets, lists } = this.props;
    const StationList = this.renderStationList;
    return (
      <Layout>
        <Head title="homepage" />

        { 
          widgets && widgets.map(
            widget => <StationList key={widget.id} list={lists && lists[widget.id]} {...widget} />
          )
        }

        <ImgTag
          src="/static/temp/970x250.png"
          render={(vs) => <img src={vs} alt="banner" style={{marginBottom: 40, cursor: 'pointer', width: '100%'}} />} />

      </Layout>
    );
  }
}

export default pageEnhance(HomePage, (state) => ({
  stations: getStations(state),
  lists: getStationLists(state)
}));
