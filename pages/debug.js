import React from 'react';

import Link from 'next/link';
// import Popout from '../util/new-popout';
import NewWindow from '../util/new-window';
import pageEnhance from '../util/page-enhance';
import Layout from '../components/layout';
import Head from '../components/head';

import Button from 'material-ui/Button';

const DEFAULT_FEATURES = {
  toolbar: 'no',
  location: 'no',
  directories: 'no',
  status: 'no',
  menubar: 'no',
  scrollbars: 'yes',
  resizable: 'yes',
  width: 500,
  height: 400,
  top: (o, w) => (w.innerHeight - o.height) / 2 + w.screenY,
  left: (o, w) => (w.innerWidth - o.width) / 2 + w.screenX
};




const newWindowFeatures = (customFeatures = {}) => {

  function toWindowFeatures(obj) {
    return Object.keys(obj)
      .reduce((features, name) => {
        let value = obj[name]
        if (typeof value === 'boolean') {
          features.push(`${name}=${value ? 'yes' : 'no'}`)
        } else {
          features.push(`${name}=${value}`)
        }
        return features
      }, [])
      .join(',')
  }

  const DEFAULT_FEATURES = {
    toolbar: 'no',
    location: 'no',
    directories: 'no',
    status: 'no',
    menubar: 'no',
    scrollbars: 'yes',
    resizable: 'yes',
    width: 500,
    height: 400,
    top: (o, w) => (w.innerHeight - o.height) / 2 + w.screenY,
    left: (o, w) => (w.innerWidth - o.width) / 2 + w.screenX
  };

  const features = Object.assign({}, DEFAULT_FEATURES, customFeatures);
  const center = 'parent';
  // Prepare position of the new window to be centered against the 'parent' window or 'screen'.
  if (typeof center === 'string' && (features.width === undefined || features.height === undefined)) {
    console.warn('width and height window features must be present when a center prop is provided')
  } else if (center === 'parent') {
    features.left = window.top.outerWidth / 2 + window.top.screenX - (features.width / 2)
    features.top = window.top.outerHeight / 2 + window.top.screenY - (features.height / 2)
  } else if (center === 'screen') {
    const screenLeft = window.screenLeft !== undefined ? window.screenLeft : screen.left;
    const screenTop = window.screenTop !== undefined ? window.screenTop : screen.top;

    const width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
    const height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

    features.left = ((width / 2) - (features.width / 2)) + screenLeft;
    features.top = ((height / 2) - (features.height / 2)) + screenTop;
  }


  return toWindowFeatures(features);

}

class DebugPage extends React.Component {
  state = {
    // isOpenPopout: false,
    isOpenNewWindow: false
  }

  // openPopout = () => this.setState({isOpenPopout: true});
  // closePopout = () => this.setState({isOpenPopout: false});

  openNewWindow = () => this.setState({isOpenNewWindow: true});
  closeNewWindow = () => this.setState({isOpenNewWindow: false});

  render() {
    return(
      <Layout>
        <Head title="Debug component" />

        <Button variant="raised" onClick={this.openNewWindow}>Open Popout</Button>

        { this.state.isOpenNewWindow && <NewWindow url="http://localhost:3000/trending?mob=1" name="debugWindowName" title="Test debug etc." copyStyles={false} /> }



        <Button variant="raised" color="primary" onClick={e => window.open('http://localhost:3000/popular?mob=1', 'Popular', newWindowFeatures())}>Open New popular Window</Button>


        <Button variant="raised" color="primary" onClick={e => window.open('http://localhost:3000/?mob=1', 'Home', newWindowFeatures())}>Open New home Window</Button>

      </Layout>
    );
  }
}

export default pageEnhance(DebugPage);