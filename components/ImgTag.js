import React from 'react';
import PropTypes from 'prop-types';
import imageChecker from '../util/image-checker';
import { CircularProgress } from 'material-ui/Progress';

const fallbackSrc = '/static/assets/images/swissradios-big.png';

class ImgTag extends React.PureComponent {
  static propTypes = {
    render: PropTypes.func.isRequired,
    src: PropTypes.string
  }
  static defaultProps = {
    src: ''
  }
  state = {
    validatedSrc: null
  }
  componentDidMount() {
    imageChecker(this.props.src)
      .then(validated => this.setState({validatedSrc: validated.url}))
      .catch(err => this.setState({validatedSrc: fallbackSrc}));
  }

  render() {
    const { validatedSrc } = this.state;
    return validatedSrc ? this.props.render(validatedSrc) : <CircularProgress />;
  }
}


export default ImgTag;