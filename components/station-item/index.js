import React from 'react';
import PropTypes from 'prop-types';
import Link from 'next/link';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import cn from 'classnames';

import Paper from 'material-ui/Paper';
import Typography from 'material-ui/Typography';
import Icon from 'material-ui/Icon';
import Grid from 'material-ui/Grid';
import Zoom from 'material-ui/transitions/Zoom';

import PlayerButton from '../player/button';
import { createSelector } from 'reselect';
import { getPlayer } from '../../core/player/selectors';
import {
  playSelected as playSelectedAction,
  pauseAudio as pauseAudioAction
} from '../../core/player/actions';

import slygify from '../../util/slugify';

import css from './station-item.css';
import ImgTag from '../ImgTag';

class StationItem extends React.PureComponent {
  static propTypes = {
    // store state
    player: PropTypes.object,
    // store dispatch
    playSelected: PropTypes.func,
    pauseAudio: PropTypes.func,
    // station
    station: PropTypes.object.isRequired,
    // styling
    zoom: PropTypes.bool,
    size: PropTypes.oneOf(['sm', 'md', 'lg']),

    //
    children: PropTypes.node
  }
  static defaultProps = {
    zoom: true,
    size: 'md',
    children: null
  }

  renderTrackInfo = () => {
    if (this.props.size === 'sm') return null;
    const { station } = this.props;
    return (
      <Grid item xs={12}>
        <div className={css.trackInfo}>
          <p>Now: {station.current}</p>
          <div>
            <Icon classes={{root: css.headsetIcon}}>headset</Icon>
            <span>&nbsp;{station.listeners}&nbsp;</span>
            <Icon classes={{root: css.trendingIcon}}>arrow_drop_down</Icon>
          </div>
        </div>
      </Grid>
    );
  }

  renderFigure = (src) => (<figure style={{backgroundImage: `url(${src})`}} />);

  render() {
    const { station, player, playSelected, pauseAudio, zoom, size } = this.props;
    const isSelected = station.id === player.stationId;

    const href = {
      pathname: '/single',
      query: {
        id: station.id,
        slug: slygify(station.title)
      }
    };

    const TrackInfo = this.renderTrackInfo;

    return (
      <Zoom in={true} timeout={zoom ? 225 : 0}>
        <Paper className={cn([css.card, css[`card-${size}`]])} elevation={size !== 'sm' ? 2 : 0}>
          <Grid container spacing={0} alignItems="center" justify="space-between">
            <Grid item xs>
              <Grid container spacing={0} alignItems="center">
                <Grid item>
                  <Link href={href}>
                    <a>
                      <ImgTag
                        src={station.artworkUrl}
                        render={this.renderFigure} />
                    </a>
                  </Link>
                </Grid>

                <Grid item xs className={cn([css.descWrap, css[`descWrap-${size}`]])}>
                  <Typography component="h3" noWrap>
                    <Link href={href}><a>{station.title}</a></Link>
                  </Typography>
                  <Typography component="p" noWrap><strong>{station.location}</strong></Typography>
                  <Typography component="p" noWrap>
                    { station.categories.map((c,i) => <React.Fragment key={i}>
                      {!!i && ', '}
                      <Link href={`/list?cat=${c.id}`}><a>{c.title}</a></Link>
                    </React.Fragment>) }
                  </Typography>
                </Grid>
              </Grid>
            </Grid>
            <Grid item className={css[`buttonWrap-${size}`]}>
              <PlayerButton
                disabled={!Boolean(station.streamUrl)}
                player={player}
                current={isSelected}
                onClick={isSelected && player.isPlaying ? pauseAudio : playSelected.bind(null, station)}
                size={size} />
            </Grid>

            <TrackInfo />

            { this.props.children && <Grid item xs={12}>
              { this.props.children }
            </Grid> }
          </Grid>

        </Paper>
      </Zoom>
    );
  }
}

const mapStateToProps = createSelector(
  getPlayer,
  (player) => ({player})
);

const mapDispatchToProps = (dispatch) => ({
  playSelected: bindActionCreators(playSelectedAction, dispatch),
  pauseAudio: bindActionCreators(pauseAudioAction, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(StationItem);