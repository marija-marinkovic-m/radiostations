import React from 'react';
import PropTypes from 'prop-types';
import { translate } from 'react-i18next';
import Typography from 'material-ui/Typography';
import Grid from 'material-ui/Grid';

const TitleBar = ({t, title, link, linkLabel = 'see all', titleComponent = 'h3', titleVariant = 'display1'}) => (
  <Grid container alignItems="center" justify="space-between" style={{marginBottom: 10}}>
    <Grid item>
      <Typography
        variant={titleVariant}
        component={titleComponent}>
        {t(title)}
      </Typography>
    </Grid>

    { link && <Grid item>
      <Typography component="p"><a href="#link">{t(linkLabel)}</a></Typography>
    </Grid> }
  </Grid>
);

TitleBar.propTypes = {
  title: PropTypes.string.isRequired,
  link: PropTypes.string,
  linkLabel: PropTypes.string,
  // connected
  t: PropTypes.func
}

export default translate(['common'])(TitleBar);