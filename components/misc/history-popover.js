import React from 'react';
import { findDOMNode } from 'react-dom';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { translate } from 'react-i18next';
import { createSelector } from 'reselect';

import { getHistory } from '../../core/history/selectors';

import Button from './flat-button';
import Icon from 'material-ui/Icon';
import Popover from 'material-ui/Popover';
import Typography from 'material-ui/Typography';

import StationItem from '../station-item';

class HistoryPopover extends React.Component {
  static propTypes = {
    // store
    history: PropTypes.object,
    // enhanced
    t: PropTypes.func
  }

  state = {
    isOpen: false,
    anchorEl: null,
    anchorOrigin: {
      vertical: 'bottom',
      horizontal: 'right'
    },
    transformOrigin: {
      vertical: 'top',
      horizontal: 'right'
    }
  }

  handleOpen = () => this.setState({
    isOpen: true,
    anchorEl: findDOMNode(this.button)
  })
  handleClose = () => this.setState({
    isOpen: false
  })

  render () {
    const { t, history } = this.props;
    const { isOpen, anchorEl, anchorOrigin, transformOrigin } = this.state;

    const historyKeys = Object.keys(history);

    return (
      <div>
        <Button
          ref={node => this.button = node}
          onClick={this.handleOpen}>
          <Icon>history</Icon>&nbsp;{t('history')}
        </Button>

        <Popover
          open={isOpen}
          anchorEl={anchorEl}
          anchorReference="anchorEl"
          anchorOrigin={anchorOrigin}
          transformOrigin={transformOrigin}
          onClose={this.handleClose}>

          { historyKeys.length === 0 && <Typography variant="subheading" style={{padding: 20}}>{t('no history')}</Typography> }

          { historyKeys.map((id) => <StationItem key={id} station={history[id]} size="sm" zoom={false} />) }
        </Popover>
      </div>
    );
  }
}

const mapStateToProps = createSelector(
  getHistory,
  (history) => ({ history })
);

const enhance = compose(
  translate(['common']),
  connect(mapStateToProps)
);

export default enhance(HistoryPopover);