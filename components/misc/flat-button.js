import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Button from 'material-ui/Button';

const styles = {
  root: {
    borderRadius: 3,
    border: 0
  },
  label: {
    textTransform: 'capitalize',
  },
};

function Classes({children, classes, ...otherProps}) {
  return (
    <Button
      variant="flat"
      size="small"
      color="secondary"
      classes={{
        root: classes.root, // className, e.g. `Classes-root-X`
        label: classes.label, // className, e.g. `Classes-label-X`
      }}
      {...otherProps}>
      {children}
    </Button>
  );
}

Classes.propTypes = {
  children: PropTypes.node,
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Classes);