import React from 'react';
import PropTypes from 'prop-types';
import { findDOMNode } from 'react-dom';
import { translate } from 'react-i18next';
import { bindActionCreators, compose } from 'redux';
import { connect } from 'react-redux';

import IconButton from 'material-ui/IconButton';
import Icon from 'material-ui/Icon';
import Popover from 'material-ui/Popover';
import List, { ListItem, ListItemIcon, ListItemText, ListItemSecondaryAction } from 'material-ui/List';
import Switch from 'material-ui/Switch';
import Collapse from 'material-ui/transitions/Collapse';
import { setCookie as setCookieAction } from '../../core/cookies/actions';

const availableLngs = {
  en: 'english',
  sr: 'serbian'
}

class OptionsPopover extends React.Component {
  static propTypes = {
    // connected
    t: PropTypes.func
  }

  state = {
    isOpen: false,
    anchorEl: null,
    anchorOrigin: {
      vertical: 'bottom',
      horizontal: 'right'
    },
    transformOrigin: {
      vertical: 'top',
      horizontal: 'right'
    },
    langExpanded: false
  }

  handleOpen = () => this.setState({
    isOpen: true,
    anchorEl: findDOMNode(this.button)
  });
  handleClose = () => this.setState({
    isOpen: false
  })

  handleToggleLang = () => this.setState({
    langExpanded: !this.state.langExpanded
  })

  handleSwitchLang = lang => this.props.setCookie(null, 'lang', lang);
  handleSwitchTheme = theme => this.props.setCookie(null, 'theme', (theme !== 'light' ? 'light' : 'dark'))

  render () {
    const { t, lang, theme } = this.props;
    const {
      isOpen, anchorEl, anchorOrigin, transformOrigin,
      langExpanded
    } = this.state;
    return (
      <div>
        <IconButton
          ref ={node => this.button = node}
          onClick={this.handleOpen}>
          <Icon>more_vert</Icon>
        </IconButton>

        <Popover
          open={isOpen}
          anchorEl={anchorEl}
          anchorReference="anchorEl"
          onClose={this.handleClose}
          anchorOrigin={anchorOrigin}
          transformOrigin={transformOrigin}>
          <List component="nav">
            <ListItem button onClick={this.handleToggleLang}>
              <ListItemIcon>
                <Icon>language</Icon>
              </ListItemIcon>
              <ListItemText primary={t(availableLngs[lang])} />
              <Icon>{ langExpanded ? 'expand_less' : 'expand_more' }</Icon>
            </ListItem>
            <Collapse in={langExpanded} timeout="auto" unmountOnExit>
              <List component="div" disablePadding>
                { 
                  Object.keys(availableLngs)
                    .filter(l => l !== lang)
                    .map(l => (
                      <ListItem
                        button onClick={this.handleSwitchLang.bind(null, l)}
                        key={l}
                        style={{paddingLeft: 30}}>
                        <ListItemText inset primary={t(availableLngs[l])} />
                      </ListItem>
                    ))
                }
              </List>
            </Collapse>

            <ListItem button>
              <ListItemIcon>
                <Icon>share</Icon>
              </ListItemIcon>
              <ListItemText primary={t('share')} />
            </ListItem>

            <ListItem button>
              <ListItemIcon>
                <Icon>help</Icon>
              </ListItemIcon>
              <ListItemText primary={t('help')} />
            </ListItem>

            <ListItem>
              <ListItemIcon>
                <Icon>
                  { theme === 'light' ? 'wb_sunny' : 'brightness_3' }
                </Icon>
              </ListItemIcon>
              {/* <ListItemSecondaryAction> */}
                <Switch
                  onChange={this.handleSwitchTheme.bind(null, theme)}
                  checked={theme === 'light'} />
              {/* </ListItemSecondaryAction> */}
            </ListItem>
          </List>
        </Popover>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  lang: state.cookies.lang,
  theme: state.cookies.theme
});
const mapDispatchToProps = dispatchEvent => ({
  setCookie: bindActionCreators(setCookieAction, dispatchEvent)
});

const enhance = compose(
  translate(['common']),
  connect(mapStateToProps, mapDispatchToProps)
);

export default enhance(OptionsPopover);