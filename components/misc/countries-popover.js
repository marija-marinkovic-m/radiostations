import React from 'react';
import PropTypes from 'prop-types';
import { findDOMNode } from 'react-dom';
import { translate } from 'react-i18next';
import { connect } from 'react-redux';
import { compose, bindActionCreators } from 'redux';

import Button from './flat-button';
import Icon from 'material-ui/Icon';
import Popover from 'material-ui/Popover';
import List, { ListItem, ListItemText, ListItemIcon } from 'material-ui/List';

import { withStyles } from 'material-ui/styles';
import { fetchApiResource } from '../../core/api/actions';
import { getResourceItems, getResourceStatus, getResourceError } from '../../core/api/selectors';
import { CircularProgress } from 'material-ui/Progress';


const API_RESOURCE = 'countries';
const API_RESOURCE_PARAMS = {active: 1};

const styles = {
  list: {
    root: {
      backgroundColor: 'black',
      color: 'white'
    }
  }
};

const countriesMock = [
  'serbia',
  'macedonia',
  'croatia',
  'montenegro',
  'bih',
  'slovenia',
];

class CountriesPopover extends React.Component {
  static propTypes = {
    t: PropTypes.func.isRequired,
    classes: PropTypes.object.isRequired,
    fetch: PropTypes.func,
    items: PropTypes.array,
    loading: PropTypes.bool,
    requestError: PropTypes.any
  }

  state = {
    isOpen: false,
    anchorEl: null,
    anchorOrigin: {
      vertical: 'bottom',
      horizontal: 'left'
    },
    transformOrigin: {
      vertical: 'top',
      horizontal: 'left'
    }
  }

  componentDidMount() {
    this.props.fetch(API_RESOURCE, API_RESOURCE_PARAMS);
  }

  handleOpen = () => this.setState({
    isOpen: true,
    anchorEl: findDOMNode(this.button)
  })
  handleClose = () => this.setState({
    isOpen: false
  })

  render() {
    if (this.props.loading) return <CircularProgress />;
    if (this.props.requestError) return <p>Countries could not be loaded.</p>;

    const { t, classes, items } = this.props;
    const { isOpen, anchorEl, anchorOrigin, transformOrigin } = this.state;
    return (
      <div>
        <List classes={{root: classes.list.root}} component="nav" dense>
          <ListItem
            button
            ref={node => this.button = node}
            onClick={this.handleOpen}>
            <ListItemText primary={t('serbia')} />
            <ListItemIcon>
              <Icon>{ isOpen ? 'keyboard_arrow_up' : 'keyboard_arrow_down' }</Icon>
            </ListItemIcon>
          </ListItem>
        </List>

        <Popover
          open={isOpen}
          anchorEl={anchorEl}
          anchorReference="anchorEl"
          onClose={this.handleClose}
          anchorOrigin={anchorOrigin}
          transformOrigin={transformOrigin}>
          <List component="nav" dense>
            {
              countriesMock.map(c => <ListItem key={c} button classes={{root: classes.list.root}}>
                <ListItemText secondary={t(c)} />
              </ListItem>)
            }
          </List>
        </Popover>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  loading: getResourceStatus(state, API_RESOURCE),
  items: getResourceItems(state, API_RESOURCE),
  requestError: getResourceError(state, API_RESOURCE)
});
const mapDispatchToProps = (dispatchEvent) => ({
  fetch: bindActionCreators(fetchApiResource, dispatchEvent)
});

export default compose(
  translate(['common']),
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps)
)(CountriesPopover);