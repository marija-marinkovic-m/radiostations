import React from 'react';
import { findDOMNode } from 'react-dom';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { translate } from 'react-i18next';
import { createSelector } from 'reselect';

import { getFavorites } from '../../core/favorites/selectors';

import Button from './flat-button';
import Icon from 'material-ui/Icon';
import Popover from 'material-ui/Popover';
import Typography from 'material-ui/Typography';

import StationItem from '../station-item';

class FavoritesPopover extends React.Component {
  static propTypes = {
    // store
    favorites: PropTypes.object,
    // enhanced
    t: PropTypes.func
  }

  state = {
    isOpen: false,
    anchorEl: null,
    anchorOrigin: {
      vertical: 'bottom',
      horizontal: 'right'
    },
    transformOrigin: {
      vertical: 'top',
      horizontal: 'right'
    }
  }

  handleOpen = () => this.setState({
    isOpen: true,
    anchorEl: findDOMNode(this.button)
  })
  handleClose = () => this.setState({
    isOpen: false
  })

  render () {
    const { t, favorites } = this.props;
    const { isOpen, anchorEl, anchorOrigin, transformOrigin } = this.state;

    const favKeys = Object.keys(favorites);

    return (
      <div>
        <Button
          ref={node => this.button = node}
          onClick={this.handleOpen}>
          <Icon>star_border</Icon>&nbsp;{t('favorites')}
        </Button>


        <Popover
          open={isOpen}
          anchorEl={anchorEl}
          anchorReference="anchorEl"
          anchorOrigin={anchorOrigin}
          transformOrigin={transformOrigin}
          onClose={this.handleClose}>

          { favKeys.length === 0 && <Typography variant="subheading" style={{padding: 20}}>{t('no favorites')}</Typography> }

          { favKeys.map((id) => <StationItem key={id} station={favorites[id]} size="sm" zoom={false} />) }
        </Popover>
      </div>
    );
  }
}

const mapStateToProps = createSelector(
  getFavorites,
  (favorites) => ({ favorites })
);

const enhance = compose(
  translate(['common']),
  connect(mapStateToProps)
);
export default enhance(FavoritesPopover);