import React from 'react';
import PropTypes from 'prop-types';
import Link from 'next/link';

import Paper from 'material-ui/Paper';
import Divider from 'material-ui/Divider';
import Typography from 'material-ui/Typography';

import css from './station-card.css';
import slygify from '../../util/slugify';
import ImgTag from '../ImgTag';

class StationCard extends React.PureComponent {
  static proptypes = {
    station: PropTypes.object.isRequired
  }

  renderFigure = (src) => (<figure><img src={src} alt="Station Logo" /></figure>);

  render() {
    const { station } = this.props;
    const href = {
      pathname: '/single',
      query: {
        id: station.id,
        slug: slygify(station.title)
      }
    };
    return (
      <Paper className={css.card}>
        <Link href={href}>
          <a>
            <ImgTag
              src={station.artworkUrl}
              render={this.renderFigure} />
          </a>
        </Link>
        <Divider />

        <Typography noWrap component="h4">
          <Link href={href}><a>{ station.title }</a></Link>
        </Typography>
        <Typography noWrap component="p"><strong>{station.location}</strong></Typography>
        <Typography noWrap component="p">
          { station.categories.map((c,i) => <React.Fragment key={i}>
            {!!i && ', '}
            <Link href={`/list?cat=${c.id}`}><a>{c.title}</a></Link>
          </React.Fragment>) }
        </Typography>
      </Paper>
    );
  }
}

export default StationCard;