import React from 'react';
import PropTypes from 'prop-types';
import Grid from 'material-ui/Grid';
import Zoom from 'material-ui/transitions/Zoom';

import StationCard from './station-card';

class StationGrid extends React.PureComponent {
  static propTypes = {
    stations: PropTypes.array.isRequired,
    size: PropTypes.oneOf(['md', 'sm']),
    zoom: PropTypes.bool
  }

  static defaultProps = {
    size: 'md',
    zoom: true
  }

  render() {
    const { stations, size, zoom } = this.props;

    const sizeProps = size === 'md' ? {sm: 4, md: 2} : {sm: 4, md: 3};

    return !stations || !stations.length ? null : (
      <Grid container spacing={24}>
        {stations.map(s => <Zoom in={true} timeout={zoom ? 225 : 0} key={s.id}>
            <Grid item xs={12} {...sizeProps}><StationCard station={s} /></Grid>
        </Zoom>) }
      </Grid>
    );
  }
}

export default StationGrid;