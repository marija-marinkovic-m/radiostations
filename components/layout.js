import React from 'react';
import Link from 'next/link';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import Icon from 'material-ui/Icon';
import IconButton from 'material-ui/IconButton';

import { withStyles } from 'material-ui/styles';
import withMui from '../mui/withRoot';

import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import Grid from 'material-ui/Grid';

import Player from './player/player';
import SearchInput from '../components/search-input';
import OptionsPopover from './misc/options-popover';
import HistoryPopover from './misc/history-popover';
import FavoritesPopover from './misc/favorites-popover';

import './layout.css';

const styles = theme => ({
  root: {
    flexGrow: 1
  },
  appFrame: {
    position: 'relative',
    zIndex: 1,
    display: 'flex',
    width: '100%',
    height: '100vh',
    overflow: 'hidden',
    maxWidth: 1360,
    margin: '0 auto'
  },
  appBar: {
    width: '100%',
    backgroundColor: theme.palette.type === 'light' ? theme.palette.background.paper : theme.palette.background.default,
    paddingLeft: theme.spacing.unit * 6,
    paddingRight: theme.spacing.unit * 7
  },
  toolbar: theme.mixins.toolbar,
  logo: {
    marginRight: 20
  },
  logoIcon: {
    fontSize: 36
  },
  content: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.default,
    paddingTop: theme.spacing.unit * 6,
    paddingRight: theme.spacing.unit * 7,
    paddingBottom: theme.spacing.unit * 15,
    paddingLeft: theme.spacing.unit * 6,
    boxSizing: 'border-box',
    overflow: 'auto'
  }
});

class Layout extends React.PureComponent {
  static propTypes = {
    classes: PropTypes.object,
    theme: PropTypes.string,
    children: PropTypes.node
  }

  render () {
    const { children, classes, theme } = this.props;
    return (
      <div className={classes.root}>
        <section className={classes.appFrame}>
          <AppBar
            position="absolute"
            className={classes.appBar}
            elevation={1}>
            <Toolbar disableGutters>
              <Grid container alignItems="center" justify="space-between" spacing={0}>
                <Grid item>
                  <Grid container alignItems="center">
                    <Grid item>
                      <Link href="/">
                        <IconButton className={classes.logo}>
                          <Icon className={classes.logoIcon}>radio</Icon>
                        </IconButton>
                      </Link>
                    </Grid>
                    <Grid item>
                  <SearchInput theme={theme} />
                    </Grid>
                  </Grid>
                </Grid>
                <Grid item>
                  <Grid container alignItems="center">
                    <FavoritesPopover />
                    <HistoryPopover />
                    <OptionsPopover />
                  </Grid>
                </Grid>
              </Grid>
            </Toolbar>
          </AppBar>
    
          <div className={classes.content}>
            <div className={classes.toolbar} />
            { children }
          </div>
    
          <Player />
        </section>
      </div>
    );
  }
}

const StyledLayout = withMui(withStyles(styles, {withTheme: true})(Layout));

const mapStateToProps = store => ({theme: store.cookies.theme});
export default connect(mapStateToProps)(StyledLayout);