import React from 'react';
import { translate } from 'react-i18next';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'next/router';
import Link from 'next/link';
import Grid from 'material-ui/Grid';
import ButtonBase from 'material-ui/ButtonBase';

import Icon from 'material-ui/Icon';

import css from './nav-bar.css';
import { getStations } from '../../core/stations/selectors';
import slygify from '../../util/slugify';

class NavBar extends React.PureComponent {
  static getDerivedStateFromProps(nextProps, prevState) {
    console.log('nextProp', nextProps);
  }

  getNext(id, stations, t) {
    if (!id) return;
    let keys = Object.keys(stations),
      idIndex = keys.indexOf(id),
      nextIndex = idIndex += 1;

    if (nextIndex >= keys.length) return null;

    const station = stations[keys[nextIndex]];

    console.log('next', station);
    if (!station) return null;

    const href = {
      pathname: '/single',
      query: {
        id: station.id,
        slug: slygify(station.title)
      }
    };

    return (
      <Link href={href}>
        <ButtonBase component="a" disableRipple className={css.nextLink}>
          {t('next')}
          <Icon>keyboard_arrow_right</Icon>
        </ButtonBase>
      </Link>
    );
  }
  getPrev(id, stations, t) {
    if (!id) return;
    let keys = Object.keys(stations),
      idIndex = keys.indexOf(id),
      prevIndex = idIndex -= 1;
    
    if (idIndex === 0) return null;

    const station = stations[keys[prevIndex]];

    console.log('prev', station);
    if (!station) return null;

    const href = {
      pathname: '/single',
      query: {
        id: station.id,
        slug: slygify(station.title)
      }
    };
    return (
      <Link href={href}>
        <ButtonBase component="a" disableRipple>
          <Icon>keyboard_arrow_left</Icon>
          {t('previous')}
        </ButtonBase>
      </Link>
    );
  }
  render () {
    const { t, router, stations } = this.props;
    const id = router.query.id;

    const PrevLink = this.getPrev.bind(null, id, stations, t);
    const NextLink = this.getNext.bind(null, id, stations, t);
    return (
      <Grid
        container
        alignItems="center"
        justify="space-between"
        className={css.bar}>
        <Grid item>
          <ButtonBase component="a" onClick={e => router.back()} disableRipple>
            <Icon>arrow_back</Icon>
          </ButtonBase>
        </Grid>
        <Grid item>
          <PrevLink />
          <NextLink />
        </Grid>
      </Grid>
    );
  }
}

export default compose(
  translate(['common']),
  withRouter,
  connect(state => ({stations: getStations(state)}))
)(NavBar);