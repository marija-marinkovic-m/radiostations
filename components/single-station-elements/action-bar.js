import React from 'react';
import PropTypes from 'prop-types';

import Grid from 'material-ui/Grid';

import FavoriteButton from './favorite-toggler';
import ShareButton from './share-button';
import WebsiteButton from './website-button';
import OptionsPopover from './options-popover';

class ActionBar extends React.PureComponent {
  static propTypes = {
    // from parent
    station: PropTypes.object.isRequired,
    // store
    favorites: PropTypes.object
    // misc
  }
  render() {
    const { station } = this.props;

    return (
      <div style={{margin: '0 5px 6px'}}>
        <Grid container
          alignItems="center"
          justify="space-between">
          <Grid item>
            <Grid container spacing={0}>
              <FavoriteButton station={station} />
              <ShareButton />
              <WebsiteButton />
            </Grid>
          </Grid>
          <Grid item>
            <OptionsPopover />
          </Grid>
        </Grid>
      </div>
    );
  }
}

export default ActionBar;