import React from 'react';
import PropTypes from 'prop-types';
import { translate } from 'react-i18next';
import { bindActionCreators, compose } from 'redux';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';

import { getFavorites } from '../../core/favorites/selectors';
import { toggleFavorite as toggleFavoriteAction } from '../../core/favorites/actions';

import Button from '../misc/flat-button';
import Icon from 'material-ui/Icon';
import IconButton from 'material-ui/IconButton';
import Snackbar from 'material-ui/Snackbar';
import Portal from 'material-ui/Portal';

class FavoriteToggler extends React.Component {

  state = {
    snackBarOpen: false,
    snackBarMessage: '',
    icon: 'star_border'
  }

  static propTypes = {
    // from parent
    station: PropTypes.object.isRequired,
    // store
    favorites: PropTypes.object,
    // misc
    t: PropTypes.func
  }

  constructor(props) {
    super(props);
    if (this.isFavorite(props)) this.state.icon = 'star';
  }

  componentWillReceiveProps(nextProps) {
    // reset icon
    // open snackbar
    if (this.props.station.id !== nextProps.station.id) {
      return this.setState({
        icon: this.isFavorite(nextProps) ? 'star' : 'star_border'
      });
    }
    
    const prevIsFavorite = this.isFavorite(this.props);
    const nextIsFavorite = this.isFavorite(nextProps);

    if (prevIsFavorite === nextIsFavorite) return;

    this.setState({
      snackBarOpen: true,
      snackBarMessage: nextIsFavorite ? 'saved to favorites' : 'removed from favorites',
      icon: nextIsFavorite ? 'star' : 'star_border'
    });
  }

  render() {
    const { icon, snackBarOpen, snackBarMessage } = this.state;
    const { station, favorites, toggleFavorite, t } = this.props;

    return (
      <span>
        <Button
          onClick={toggleFavorite.bind(null, station)}
          aria-label="Toggle Favorite">
          <Icon>{icon}</Icon>
          &nbsp;{t('favorite')}
        </Button>
        <Portal>
        <Snackbar
          open={snackBarOpen}
          anchorOrigin={{
            vertical: 'top',
            horizontal: 'right'
          }}
          autoHideDuration={6000}
          onClose={this.handleCloseSnackBar}
          ContentProps={{
            'aria-describedby': 'favorites-message'
          }}
          message={<span id="favorites-message">{t(snackBarMessage)}</span>}
          action={[
            <IconButton
              key="close"
              aria-label="Close"
              color="inherit"
              onClick={this.handleCloseSnackBar}>
              <Icon>close</Icon>
            </IconButton>
          ]} />
        </Portal>
      </span>
    );
  }

  handleCloseSnackBar = (event, reason) => {
    if (reason === 'clickaway') return;
    this.setState({snackBarOpen: false});
  }

  isFavorite = (props) => props.favorites &&
    props.station &&
    Object.keys(props.favorites)
      .indexOf(props.station.id) > -1;
}

const mapStateToProps = createSelector(
  getFavorites,
  (favorites) => ({ favorites })
);

const mapDispatchToProps = dispatchEvent => ({
  toggleFavorite: bindActionCreators(toggleFavoriteAction, dispatchEvent)
});

const enhance = compose(
  translate(['common']),
  connect(mapStateToProps, mapDispatchToProps)
);

export default enhance(FavoriteToggler);