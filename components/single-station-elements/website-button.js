import React from 'react';
import { translate } from 'react-i18next';
import { compose } from 'redux';
import { withRouter } from 'next/router';
import { getStations } from '../../core/stations/selectors';
import { connect } from 'react-redux';

import Icon from 'material-ui/Icon';
import Button from '../misc/flat-button';

class WebsiteButton extends React.PureComponent {
  render() {
    const { t, router:{ query: {id} }, stations } = this.props;
    const station = stations[id];
    return station && station.website ? (
      <Button component="a" href={station.website} target="_blank">
        <Icon>web</Icon>
        &nbsp;{t('website')}
      </Button>
    ) : null;
  }
}

// export default translate(['common'])(WebsiteButton);

export default compose(
  translate(['common']),
  withRouter,
  connect(state => ({stations: getStations(state)}))
)(WebsiteButton);