import React from 'react';
import { translate } from 'react-i18next';

import Icon from 'material-ui/Icon';
import Button from '../misc/flat-button';

class ShareButton extends React.PureComponent {
  render() {
    const { t } = this.props;
    return (
      <div>
        <Button>
          <Icon>share</Icon>
          &nbsp;{t('share')}
        </Button>
      </div>
    );
  }
}

export default translate(['common'])(ShareButton);