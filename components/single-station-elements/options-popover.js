import React from 'react';
import PropTypes from 'prop-types';
import { findDOMNode } from 'react-dom';
import { translate } from 'react-i18next';

import IconButton from 'material-ui/IconButton';
import Icon from 'material-ui/Icon';
import Popover from 'material-ui/Popover';
import List, { ListItem, ListItemIcon, ListItemText, ListItemSecondaryAction } from 'material-ui/List';

class OptionsPopover extends React.Component {
  state = {
    isOpen: false,
    anchorEl: null,
    anchorOrigin: {
      vertical: 'bottom',
      horizontal: 'right'
    },
    transformOrigin: {
      vertical: 'top',
      horizontal: 'right'
    }
  }

  handleOpen = () => this.setState({
    isOpen: true,
    anchorEl: findDOMNode(this.button)
  });
  handleClose = () => this.setState({
    isOpen: false
  })

  render() {
    const { isOpen, anchorEl, anchorOrigin, transformOrigin } = this.state;
    const { t } = this.props;

    return (
      <div>
        <IconButton
          ref={node => this.button = node}
          onClick={this.handleOpen}>
          <Icon>more_vert</Icon>
        </IconButton>
        <Popover
          open={isOpen}
          anchorEl={anchorEl}
          anchorReference="anchorEl"
          onClose={this.handleClose}
          anchorOrigin={anchorOrigin}
          transformOrigin={transformOrigin}>
          <List component="nav">
            <ListItem button dense>
              <ListItemIcon><Icon>warning</Icon></ListItemIcon>
              <ListItemText primary={t('report')} />
            </ListItem>
            <ListItem button dense>
              <ListItemIcon><Icon>mode_edit</Icon></ListItemIcon>
              <ListItemText primary={t('edit')} />
            </ListItem>
          </List>
        </Popover>
      </div>
    );
  }
}

export default translate(['common'])(OptionsPopover);