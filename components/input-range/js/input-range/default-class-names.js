import cn from 'classnames';
import css from '../../css/input-range.css'

/**
 * Default CSS class names
 * @ignore
 * @type {InputRangeClassNames}
 */
const DEFAULT_CLASS_NAMES = {
  activeTrack: cn([css['input-range__track'], css['input-range__track--active']]),
  disabledInputRange: cn([css['input-range'], css['input-range--disabled']]),
  inputRange: css['input-range'],
  slider: css['input-range__slider'],
  sliderContainer: css['input-range__slider-container'],
  track: cn([css['input-range__track'], css['input-range__track--background']])
};

export default DEFAULT_CLASS_NAMES;
