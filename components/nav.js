import React from 'react';
import PropTypes from 'prop-types';

import Head from './head';
import Link from 'next/link';
import { withRouter } from 'next/router';
import { translate } from 'react-i18next';
import { connect } from 'react-redux';
import { compose, bindActionCreators } from 'redux';

import { isSetSidebarNav } from '../core/api/selectors';
import { setNav as setNavAction } from '../core/api/actions';

import List, { ListItem, ListItemIcon, ListItemText } from 'material-ui/List';
import Divider from 'material-ui/Divider';
import Icon from 'material-ui/Icon';

import CountriesPopover from './misc/countries-popover';

import getLinkHref from '../util/get-link-href';
import { CircularProgress } from 'material-ui/Progress';

const LinkElement = ({pathname, icon, title, nextQuery, t, router: { pathname: routerPathname, query }}) => {
  return (
    <Link prefetch href={getLinkHref(query, pathname, nextQuery)}>
      <ListItem button component="a" dense={true}>
        { icon && <ListItemIcon classes={{root: routerPathname === pathname ? 'text-blue' : 'text-light'}}><Icon>{ icon }</Icon></ListItemIcon> }
        <ListItemText primary={t(title.toLowerCase())} classes={{primary: 'text-light text-capitalize'}} />
      </ListItem>
    </Link>
  )
};

export const ActiveLinkElement = withRouter(LinkElement);

class NavComponent extends React.PureComponent {
  static propTypes = {
    // redux state
    nav: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]).isRequired,
    // router
    router: PropTypes.object.isRequired,
    // i18n
    t: PropTypes.func.isRequired
  };
  componentWillMount() {
    const { nav } = this.props;

    /**
     * @todo: need to provide some sccreen something for user to select 'nationality' if no nationalitySlug provided
     */
    const nationalitySlug = (this.props.router.query && this.props.router.query.nationalitySlug) || 'srbija';

    if (nav !== false && nav.nationalitySlug === nationalitySlug) return;
    this.props.setNav(nationalitySlug);
  }

  renderList = ({list}) => {
    if (!list) return null;
    return (<List component="nav" dense={true}>
      { list.map(item => <ActiveLinkElement key={item.id} t={this.props.t} {...item} />) }
    </List>);
  }

  render() {
    const { nav } = this.props;

    if (!nav || !nav.links || nav.links.pending) return <CircularProgress />;

    const { main, genres, locations } = nav.links;
    const TheList = this.renderList;
    return (
      <nav>
        <CountriesPopover />
        <TheList list={main} />

        { locations && 'Locations' }

        <TheList list={locations} />

        { genres && 'Genres' }
        <TheList list={genres} />
      </nav>
    );
  }
}

const Nav = ({t}) => (
  <nav>
    <CountriesPopover />
    <List component="nav" dense={true}>
      { mainNav.map((item, i) => <ActiveLinkElement key={i} t={t} {...item} />) }
    </List>

    <style jsx>{`
      :global(body) {
        margin: 0;
      }
    `}</style>
  </nav>
);

const mapStateToProps = (state) => ({
  nav: isSetSidebarNav(state)
});
const mapDispatchToProps = (dispatchEvent) => ({
  setNav: bindActionCreators(setNavAction, dispatchEvent)
});


export default compose(
  translate(['common']),
  withRouter,
  connect(mapStateToProps, mapDispatchToProps)
)(NavComponent);

// export default translate(['common'])(Nav);