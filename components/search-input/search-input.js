import React from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { findDOMNode } from 'react-dom';
import { translate } from 'react-i18next';
import Downshift from 'downshift';

import IconButton from 'material-ui/IconButton';
import Icon from 'material-ui/Icon';
import Paper from 'material-ui/Paper';
import StationItem from '../station-item'

import debounce from '../../util/debounce';
import { api } from '../../core/api/api-service';

import css from './search-input.css';
import { createStation } from '../../core/stations/reducer';
import { withStyles } from 'material-ui/styles';
import { CircularProgress } from 'material-ui/Progress';

const debounceTime = 300;

const styles = theme => ({
  container: {
    position: 'relative'
  },
  paper: {
    position: 'absolute',
    zIndex: 1,
    marginTop: theme.spacing.unit,
    left: 0, right: 0
  }
});

class SearchAutocomplete extends React.Component {
  static propTypes = {
    t: PropTypes.func.isRequired,
    theme: PropTypes.string.isRequired,
    classes: PropTypes.object.isRequired
  }

  state = {
    items: [],
    loading: false,
    error: null
  }

  fetchResults = debounce(name => {
    this.setState({loading: true}, () => {
      api.fetchStations({query: name})
        .then(({collection}) => this.setState({
          loading: false,
          items: collection.map(s => createStation(s))
        }))
        .catch(err => this.setState({
          loading: false,
          items: [],
          error: err
        }));
    });
  }, debounceTime);

  renderItems = (selectedItem, getItemProps, highlightedIndex) => {
    const { items } = this.state;
    return (
      <div>
        { items.map((item, index) => {
          const isHighlighted = highlightedIndex === index;
          const itemProps = getItemProps({
            item,
            style: {
              backgroundColor: isHighlighted ? 'gray' : 'white',
              fontWeight: selectedItem && selectedItem.title === item.title ? 'bold': 'normal'
            }
          });
          return <StationItem
            {...itemProps}
            key={item.id}
            station={item}
            size="sm" zoom={false} />;
        }) }
      </div>
    );
  }

  renderDownshift = ({
    selectedItem,
    getInputProps,
    getItemProps,
    highlightedIndex,
    isOpen
  }) => {
    const { loading } = this.state;
    const { theme, t, classes } = this.props;

    const Items = this.renderItems.bind(null, selectedItem, getItemProps, highlightedIndex);
    const inputProps = getInputProps({
      id: 'radio-app-input-search-id',
      placeholder: t('search'),
      ref: node => {this._input = node},
      onChange: event => {
        const value = event.target.value;
        if (!value) return;
        this.fetchResults(value);
      }
    });
    return (<div className={theme !== 'light' ? css.darkSearch : css.search}>
      <input {...inputProps} />
      <IconButton className={theme !== 'light' ? css.darkIcon : css.icon}>
        { !loading ? <Icon>search</Icon> : <CircularProgress size={24} /> }
      </IconButton>
      <div className={classes.container}>
        { isOpen ? (
          <Paper className={classes.paper} square>
            <Items />
          </Paper>
        ) : null }
      </div>
    </div>);
  }

  render() {
    return (
      <Downshift
        render={this.renderDownshift}
        itemToString={s => s == null ? '' : s.title} />
    );
  }
}

export default compose(
  withStyles(styles),
  translate(['common']),
  connect(state => ({theme: state.cookies && state.cookies.theme || 'light'}))
)(SearchAutocomplete);