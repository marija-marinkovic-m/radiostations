import PropTypes from 'prop-types';
import cn from 'classnames';

import Button from 'material-ui/ButtonBase';
import Icon from 'material-ui/Icon';

import css from './button.css';

const PlayerButton = ({player, size = 'sm', onClick = () => {}, current = true, disabled = false}) => {
  const { isReady, stationId, isPlaying } = player;
  const buttonIcon = !current ? 'play_arrow' : !isReady ? 'cached' : isPlaying ? 'pause' : 'play_arrow';
  return (
    <Button
      disabled={disabled}
      classes={{root: css.button}}
      className={cn([css[size]])}
      variant="fab"
      aria-label="Player button"
      color="primary"
      onClick={onClick}>
      <Icon className={cn([css[`${size}Icon`]], { [css.spinIcon]: buttonIcon === 'cached'})}>{ buttonIcon }</Icon>
    </Button>
  );
}

PlayerButton.propTypes = {
  disabled: PropTypes.bool,
  onClick: PropTypes.func,
  size: PropTypes.oneOf(['sm', 'md', 'lg']),
  player: PropTypes.shape({
    isReady: PropTypes.bool,
    isPlaying: PropTypes.bool,
    stationId: PropTypes.oneOfType([PropTypes.number, PropTypes.string])
  }).isRequired
}

export default PlayerButton;