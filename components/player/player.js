import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createSelector } from 'reselect';
import { getPlayer } from '../../core/player/selectors';
import { getFavorites } from '../../core/favorites/selectors';

import Button from './button';
import InputRange from '../input-range';

import css from './player.css';
import { getStationById, getStations } from '../../core/stations/selectors';
import {
  pauseAudio as pauseAudioAction,
  playSelected as playSelectedAction,
  setVolume as setVolumeAction
} from '../../core/player/actions';


import Typography from 'material-ui/Typography';
import Grid from 'material-ui/Grid';
import Icon from 'material-ui/Icon';
import IconButton from 'material-ui/IconButton';
import ImgTag from '../ImgTag';

class PlayerComponent extends React.Component {
  state = {
    expanded: false,
    playerExpandedAnimationClass: 'slideInUp'
  }

  toggleExpand = () => this.setState((prevState) => {
    return prevState.expanded ? ({playerExpandedAnimationClass: css.slideOutDown}) : ({expanded: true, playerExpandedAnimationClass: css.slideInUp});
  }, () => {
    setTimeout(() => {
      if (this.state.playerExpandedAnimationClass === css.slideOutDown) this.setState({expanded: false})
    }, 1000);
  })

  handlePlay = () => {
    const {
      player: { isPlaying },
      pauseAudio, playSelected,
      station
    } = this.props;

    if (isPlaying) {
      return pauseAudio()
    }
    playSelected(station)
  }

  handleVolumeIncrease = () => this.props.setVolume(this.props.player.volume + 0.05)

  handleVolumeDecrease = () => this.props.setVolume(this.props.player.volume - 0.05)

  renderFigure = (src) => (
    <figure
      className={css.figure}
      style={{backgroundImage: `url(${src})`}}
      onClick={this.toggleExpand} />
  )
  renderFigureExpanded = (src) => (
    <figure
      className={css.figureExpanded}
      style={{backgroundImage: `url(${src})`}} />
  );

  renderPlayer = () => {
    const { station, player } = this.props;
    return (
      <div className={css.player}>

        <Grid container alignItems="center">
          <Grid item xs zeroMinWidth classes={{ item: css.textCenter }}>
            <ImgTag
              src={station.artworkUrl}
              render={this.renderFigure} />
          </Grid>
          <Grid item md={10} sm={9} xs={7}>
            <div
              className={css.description}
              onClick={this.toggleExpand}>
              <h4 className={css.stationTitle}>{station.title}</h4>
              <p className={css.stationCurrent}>{station.current}</p>
            </div>
          </Grid>
          <Grid item xs zeroMinWidth classes={{ item: css.textCenter }}>
            <Button
              player={player}
              onClick={this.handlePlay} />
          </Grid>
        </Grid>

      </div>
    );
  }
  renderPlayerExpanded = () => {
    const { station, player, setVolume } = this.props;
    return (
      <div>
        <div className={cn([this.state.playerExpandedAnimationClass, css.playerExpanded])}>
          <div className={css.playerExpandedWrap}>
            <Grid container alignItems="stretch" style={{height: '100%'}}>
              <Grid item xs={12}>
                <IconButton onClick={this.toggleExpand}>
                  <Icon color="secondary">keyboard_arrow_down</Icon>
                </IconButton>
              </Grid>
              <Grid item xl={12} xs={12}>
                <ImgTag
                  src={station.artworkUrl}
                  render={this.renderFigureExpanded} />
                <h4 className={css.stationTitle}>{station.title}</h4>
                <p className={css.stationCurrent}>{station.current}</p>
              </Grid>

              <Grid item xl={12} xs={12}>
                <Grid container alignContent="center" alignItems="center">
                  <Grid item xs>
                    <IconButton>
                      <Icon color="secondary" style={{fontSize:14}}>warning</Icon>
                    </IconButton>
                  </Grid>
                  <Grid item xs>
                    <IconButton>
                      <Icon color="secondary" className={css.lightBorder}>chevron_left</Icon>
                    </IconButton>
                  </Grid>
                  <Grid item xs>
                    <Button
                      player={player}
                      size="md"
                      onClick={this.handlePlay} />
                  </Grid>
                  <Grid item xs>
                    <IconButton>
                      <Icon color="secondary" className={css.lightBorder}>chevron_right</Icon>
                    </IconButton>
                  </Grid>
                  <Grid item xs>
                    <IconButton>
                      <Icon color="secondary" style={{fontSize: 16}}>sync</Icon>
                    </IconButton>
                  </Grid>
                </Grid>
              </Grid>

              <Grid item xl={12} xs={12}>
                <Grid container alignContent="center" alignItems="center">
                  <Grid item xs zeroMinWidth>
                    <IconButton onClick={this.handleVolumeDecrease}>
                      <Icon style={{color: 'white'}}>volume_mute</Icon>
                    </IconButton>
                  </Grid>
                  <Grid item xs={8}>
                    <InputRange
                      maxValue={1}
                      minValue={0}
                      step={0.05}
                      value={player.volume}
                      onChange={setVolume} />
                  </Grid>
                  <Grid item xs zeroMinWidth>
                    <IconButton onClick={this.handleVolumeIncrease}>
                      <Icon style={{color: 'white'}}>volume_up</Icon>
                    </IconButton>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </div>

        </div>
        <div className={css.playerBox} />
      </div>
    );
  }
  render() {
    const { station } = this.props;
    return station && station.id ? this.state.expanded ? this.renderPlayerExpanded() : this.renderPlayer() : null;
  }
}

const mapStateToProps = createSelector(
  getPlayer,
  getFavorites,
  getStations,
  (player, favorites, stations) => ({player, favorites, station: stations && stations[player.stationId] || {}})
);

const mapDispatchToProps = dispatchEvent => ({
  playSelected: bindActionCreators(playSelectedAction, dispatchEvent),
  pauseAudio: bindActionCreators(pauseAudioAction, dispatchEvent),
  setVolume: bindActionCreators(setVolumeAction, dispatchEvent)
});

export default connect(mapStateToProps, mapDispatchToProps)(PlayerComponent);