import React from 'react';
import PropTypes from 'prop-types';
import { MuiThemeProvider } from 'material-ui/styles';
import getPageContext from './getPageContext';
import { createMuiTheme } from 'material-ui/styles';

import purple from 'material-ui/colors/purple';
import grey from 'material-ui/colors/grey';

const theme = createMuiTheme({
  palette: {
    type: 'light',
    primary: {
      light: purple[300],
      main: purple[500],
      dark: purple[700],
    },
    secondary: {
      light: grey[300],
      main: grey[500],
      dark: grey[700],
    },
    background: {
      default: '#F3F4F5'
    }
  },
});

const darkTheme = createMuiTheme({
  palette: {
    type: 'dark',
    secondary: {
      light: grey[300],
      main: grey[500],
      dark: grey[700],
    },
  },
});

function withRoot(Component) {
  class WithRoot extends React.Component {
    componentWillMount() {
      this.pageContext = this.props.pageContext || getPageContext();
    }

    componentDidMount() {
      // Remove the server-side injected CSS.
      const jssStyles = document.querySelector('#jss-server-side');
      if (jssStyles && jssStyles.parentNode) {
        jssStyles.parentNode.removeChild(jssStyles);
      }
    }

    pageContext = null;

    render() {
      // MuiThemeProvider makes the theme available down the React tree thanks to React context.
      return (
        <MuiThemeProvider
          theme={this.props.theme !== 'dark' ? theme : darkTheme}
          sheetsManager={this.pageContext.sheetsManager}
        >
          <Component {...this.props} />
        </MuiThemeProvider>
      );
    }
  }

  WithRoot.propTypes = {
    pageContext: PropTypes.object,
    theme: PropTypes.string
  };

  WithRoot.getInitialProps = ctx => {
    if (Component.getInitialProps) {
      return Component.getInitialProps(ctx);
    }

    return {};
  };

  return WithRoot;
}

export default withRoot;