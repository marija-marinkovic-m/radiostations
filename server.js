const express = require('express');
const next = require('next');
const { parse, format } = require('url');

const port = parseInt(process.env.PORT, 10) || 3000;
const dev = process.env.NODE_ENV !== 'production';
const app = next({ dev });
const handle = app.getRequestHandler();

app
  .prepare()
  .then(() => {
    const server = express();

    //-- index /[{nationality}]
    //-- popular /{nationality}/radio-stanice
        //-- genre /{nationality}/radio-stanice?zanr={genreSlug}
        //-- location /{nationality}/radio-stanice?lokacija={locationSlug}
    //-- trending /{nationality}/uzivo
    //-- single /radio/{stationId}/{stationSlug}

    // popular -- genre (zanr), location (lokacija)
    server.get('/:nationality/radio-stanice', (req, res) => {
      return app.render(req, res, '/popular', Object.assign({}, req.query, {
        nationalitySlug: req.params.nationality,
        genre: req.query.zanr,
        location: req.query.lokacija
      }));
    });

    // trending
    server.get('/:nationality/uzivo', (req, res) => {
      return app.render(req, res, '/trending', Object.assign({}, req.query, {
        nationalitySlug: req.params.nationality
      }));
    });

    // single
    server.get('/radio/:id/:slug?', (req, res) => {
      return app.render(req, res, '/single', Object.assign({}, req.query, {
        id: req.params.id,
        slug: req.params.slug
      }));
    });

    // index
    server.get('/:nationality?', (req, res) => {
      return app.render(req, res, '/', Object.assign({}, req.query, {
        nationalitySlug: req.params.nationality
      }));
    });


    /*
    * @tbd: parametrized routing
    * 
    * server.get('/test/:id', (req, res) => {
    *   return app.render(req, res, '/test', Object.assign({id: req.params.id}, req.query))
    * })
    * */

    // server.get('/other', (req, res) => app.render(req, res, '/other'));

    /**
     * @todo:
     * express detect device and serve appropriate pages via specific url param
     * (mob | mode | m | etc.)
     */

    // layout param
    // server.get('/m(/*)?', (req, res) => {
    //   const parsedUrl = parse(req.url, true);
    //   console.log('accessing m mode... \n Parsed url:', parsedUrl);
    //   let pathname = parsedUrl.pathname.length > 2 ? parsedUrl.pathname.substr(2, parsedUrl.pathname.length) : '/';
    //   res.redirect(format({
    //     pathname,
    //     query: Object.assign({}, parsedUrl.query, {mob: 1})
    //   }));
    // });

    // SINGLE station
    // server.get('/radio/:id/:slug?', (req, res) => {
    //   return app.render(req, res, '/single-station', Object.assign({id: req.params.id, slug: req.params.slug}, req.query));
    // });
    
    server.get('*', (req, res) => handle(req, res));

    server.listen(port, err => {
      if (err) throw err;
      console.log(`> Ready on http://localhost:${port}`);
    })
  });